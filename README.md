# Notch Filter and Locally Optimized Combination of Rotations detrending algorithms for lightcurve data and planet transit search

Packaged version of the K2 and TESS mission young stars detrending algorithms developed Aaron C. Rizzuto (Rizzuto et al. 2017 https://arxiv.org/abs/1709.09670). 
ACR will update this by copying from his working repository when it's needed.

Packages you need:

Aaron's pyutils repository in your python path.

Others:
mpyfit: https://github.com/evertrol/mpyfit
Laura Kreidebrg's batman pipeline if you want injections: https://astro.uchicago.edu/~kreidberg/batman/
Box Least Squares (BLS) wrapped for python: https://github.com/dfm/python-bls
Good old scipy: https://www.scipy.org/

Everything you need to know to run the code is in example_script.py,
which sould just work out of the gate once the above packages are
ready.




