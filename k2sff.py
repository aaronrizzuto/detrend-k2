import numpy as np
from scipy.io.idl import readsav as idlread
##from pydl.pydlutils import bspline ##now using my version without the maskpoints problems, imported below
import numpy.lib.recfunctions
import warnings

##my imports 
from robust_mean import robustmean
import bspline_acr as bspline

def robustpolyfit(t,f,degree):    
    lastgood = np.arange(len(t))
    for i in range(6):
        thist = t[lastgood]
        thisf = f[lastgood]
        
        polyc = np.polyfit(thist,thisf,degree)
        bg    = np.polyval(polyc,t)    
        j1,j2,j3,goodind = robustmean(f-bg,3)
        rms   = np.std(bg[goodind]-f[goodind])
        
        if len(lastgood) == len(goodind):
            if np.sum(lastgood != goodind) == 0: break
        
        lastgood = goodind.copy()
    
    return bg,rms,goodind,polyc
    
def cdpp(lc,hours=6,outcdppt = False):
    naverage = hours*2+1
    cdppt = np.zeros(len(lc)-naverage)
    for i in range(len(lc)-naverage):
        cdppt[i] = 1e6*np.std(lc[i:i+naverage+1])
    if outcdppt == True: return np.nanmedian(cdppt)/np.sqrt(naverage),cdppt
    return np.nanmedian(cdppt)/np.sqrt(naverage)
    

def outlierbinrej(arclength,fforcor,nbinal,sigma,dostop=False,thelogfile=None):
    goodpoints = np.array([-1])
    maxal = np.nanmax(arclength)
    minal = np.nanmin(arclength)
    increment = (maxal-minal)/nbinal
    for ii in range(nbinal):
        lowerlim = ii*increment+minal
        upperlim = (ii+1)*increment+minal
        thisbin = np.where((arclength < upperlim) & (arclength >= lowerlim) & (np.isfinite(fforcor)))[0]
        if ii == nbinal-1: thisbin = np.where((arclength >= lowerlim) & (np.isfinite(fforcor)))[0] ##make sure floating errors dont exclude largets valure from final bin
        if (len(thisbin) != 0) & (len(np.unique(arclength[thisbin]))  >= 2): ##make sure there's at least two unique values in the bin, actually make that 3, two points are never going to result in a rejection anyway
            if thelogfile != None:
                thelogfile.write('Up to interim ' + str(ii) + ' \n')
                #thelogfile.write(str(len(thisbin)) + ' \n ') 
                thelogfile.write(np.array_str(thisbin) + ' ' + np.array_str(arclength[thisbin]) + ' ' + np.array_str(fforcor[thisbin]) +'_'+ np.array_str(np.isfinite(fforcor[thisbin])) + ' \n ') 
                thelogfile.flush()
            interim1 = np.polyfit(arclength[thisbin],fforcor[thisbin],1)
            if thelogfile != None:
                thelogfile.write('Up to interim2 ' + str(ii) + ' \n')
                thelogfile.flush()
            interim = np.polyval(interim1,arclength[thisbin])
            if thelogfile != None:
                thelogfile.write('outlier interim done ' + str(ii) + ' \n')
                thelogfile.flush()
            jmean,jsigma,jnumrej,goodind = robustmean(fforcor[thisbin]-interim,sigma)
            ##jmean,jsigma,jnumrej,goodind = robustmean(fforcor[thisbin]-np.polyval(np.polyfit(arclength[thisbin],fforcor[thisbin],1),arclength[thisbin]),sigma)
            if thelogfile != None:
                thelogfile.write('outlier rmean done ' + str(ii) + ' \n')
                thelogfile.flush()

            goodpoints = np.append(goodpoints,thisbin[goodind])
            #if dostop == True: 
            #    import pdb
            #    pdb.set_trace()
    goodpoints = goodpoints[1:]

    return goodpoints

def diff(arr):
    n = len(arr)
    delta = np.append(np.array([0.0]),arr[1:n]-arr[0:n-1])
    delta[0] = delta[1]
    return delta
    
def medianfilter(y,n,reflect=False,robust_mean=False):
    n=2*n/2
    if reflect == True:
        y2 = np.concatenate((y[::-1],y,y[::-1]))
        yn = y.copy()
        ns = len(y)
        
        for i in range(len(y)):
            yn[i] = np.nanmedian(y2[i+ns-n/2:i+ns+n/2+1])
            if robust_mean == True: yn[i] = robustmean(y2[i+ns-n/2:i++ns+n/2+1],2)
    
    if reflect == False:
        yn = y.copy()
        for i in range(n/2,len(y)):
            yn[i] = np.nanmedian(y[i-n/2:i+n/2+1])
            if robust_mean == True: yn[i] = robustmean(y[i-n/2:i+n/2+1],3)
        
    return yn
 
#@profile
def lcbin(time2,lc,nbins,usemean=False,userobustmean=False,linfit=False):      

    time = time2-np.nanmin(time2)
    
    usemedian = (1-usemean) & (1-userobustmean)
    
    binsize = (np.nanmax(time)-np.nanmin(time))/float(nbins)
    fbin    = np.empty(nbins)*np.nan
    ebin    = np.empty(nbins)*np.nan
    tbin    = (np.arange(nbins,dtype=float)+0.5)*binsize + np.nanmin(time2)
    if  (userobustmean == True) or (linfit==True): allgoodind = np.array([-1])


    for i in range(nbins):
        w = np.where((time >= i*binsize) & (time < (i+1)*binsize))[0]
        if len(w) > 0:
            if usemedian == True:
                fbin[i] = np.nanmedian(lc[w])
                if len(w) > 1: ebin[i] = 1.253*np.std(lc[w])/np.sqrt(len(w))
            
            if usemean == True:
                fbin[i] = np.nanmean(lc[w])
                if len(w) > 1: ebin[i] = np.std(lc[w])/np.sqrt(len(w))
            
            if userobustmean == True:
                fbin[i], j1,j2,goodind = robustmean(lc[w],3)
                if len(w) > 1:
                    if len(goodind) > 1: 
                        ebin[i] = np.std(lc[w[goodind]])/np.sqrt(len(w))
                    else: ebin[i] = np.inf
                allgoodind = np.concatenate((allgoodind,w[goodind]))
        
            if linfit == True:
                if len(w) > 2:
                    bg,rms,goodind,polyfit = robustpolyfit(time2[w],lc[w],1)
                    fbin[i] = np.polyval(polyc,tbin[i])
                    ebin[i] = rms/np.sqrt(len(w))
                    allgoodind = np.concatenate((allgoodind,w[goodind]))
                if len(w) <= 1: ##if not enough points revert to robust mean
                    fbin[i],j1,j2,goodind = robustmean(lc[w],3,goodind)
                    if len(w) >1:  ##this doesnt make sense!!!!
                        ebin[i] = np.std(lc[w[goodind]])/np.sqrt(len(w))
                        allgoodind = np.concatenate((allgoodind,w[goodind]))
                
    
    if robustmean == True: allgoodind = allgoodind[1:]
    return fbin,binsize,ebin,allgoodind,tbin
                                           
def inttrap(x,y,cumulative = False):
    n     = len(x)
    sums  = np.append(np.array([0.0]),y[1:n]+y[0:n-1])
    diffs = diff(x)
    integ = sums*0.5*diffs
    if cumulative == False: return np.sum(integ)
    if cumulative == True : return np.cumsum(integ) 

#vanderburgs keplersline, without the yplot or breakp options
def keplerspline(t,f,ndays=1.5,maxiter=6,breakp=False,thisstop=False):
    
    gaps = np.where(diff(t) > ndays)[0]
    if len(gaps) == 0: s = keplerspline2(t,f,ndays=ndays,maxiter=maxiter,thisstop=thisstop) #make sure we separate the continuum normalizations between gaps in data
    if len(gaps)  > 0:
        s = np.zeros(len(t))
        gaps = np.concatenate((np.array([0]),gaps,np.array([len(t)])))
        #print "in gaps"
        for i in range(1,len(gaps)):
            s[gaps[i-1]:gaps[i]] = keplerspline2(t[gaps[i-1]:gaps[i]],f[gaps[i-1]:gaps[i]],ndays=ndays,maxiter=maxiter,thisstop=thisstop)
    return s
  
##vanderburgs keplerspline2 without yplot option
def keplerspline2(t,f,ndays=1.5,maxiter=6,verbose=False,thisstop = False):  
    t2       = (t-np.nanmin(t))/(np.nanmax(t)-np.nanmin(t))
    bksp     = ndays/(np.nanmax(t)-np.nanmin(t))
    lastgood = np.arange(len(t2))
    for i in range(maxiter):            
        bg   = bspline.iterfit(t2[lastgood],f[lastgood],bkspace=bksp,nord=4,maxiter=0)[0].value(t2)[0]
        good = robustmean(f-bg,3)[3]
        rms  = np.std(bg[good]-f[good])
            
        ##skipping plotting here
        
        
        if len(lastgood) == len(good):
            if np.sum(lastgood != good) == 0:
                if verbose == True: print 'converged in ' + str(i+1) + ' iterations'
                break
        lastgood=good.copy()
    return bg
    
    
##function that calls the k2sff reduction for the raw to rawrev step
def reducek2_rawrev2rev(rawdata,datadir='k2data/',cnum = 4,supresswarn=True,numdays=1.5,numdaysfirst=1.5,gradual=False,idstring='',thelogfile=None):
    if supresswarn == True: warnings.simplefilter('ignore',np.RankWarning) ##hides numpy polyfit warnings for poor conditioning
    
    if idstring == '0': print 'Setting my Parameters to: '+ str(numdays) + ' ' + str(numdaysfirst) + ' ' + str(gradual)
    #k2sff_log = open('logfiles/k2sfflog.txt','ab')
    ##get the right centroid star for each campaign
    if cnum == 4 : centroidepic = 210331908   
    if cnum == 2 : centroidepic = 205651741
    if cnum == 5 : centroidepic = 211307639 
    if cnum == 13: centroidepic = 210561006
    ##first restore the centroid estimate star data 
    #k2sff_log.write(idstring + ' Up to centroid read'+ ' \n')
    #k2sff_log.flush()
    stuff = idlread(datadir+'rawrev/ktwo'+str(centroidepic)+'extr.idl',python_dict=True)
#    Available variables:
# - circmasks [<type 'numpy.ndarray'>]
# - timage [<type 'numpy.ndarray'>]
# - modulenum [<type 'numpy.int32'>]
# - channelnum [<type 'numpy.int32'>]
# - psfmasks [<type 'numpy.ndarray'>]
# - xcenter [<type 'numpy.ndarray'>]
# - flag [<type 'str'>]
# - rawdata [<class 'numpy.recarray'>]
# - ycenter [<type 'numpy.ndarray'>]
    newxc = stuff['rawdata'].xcms
    newyc = stuff['rawdata'].ycms        
    stuff = 0.0 ##clear the memory
    
    ##we should do some very basic cleaning here, for crazy outliers:
    ##we should probably remove anything significantly above the light curve
    ##just pick a random aperture
    psffluxer  = np.squeeze(np.dstack(rawdata.fpsf).T*1.0)
    qq = 4
    moff = -np.nanmedian(psffluxer[:,qq]) + psffluxer[:,qq]
    md = np.nanmedian(np.absolute(moff))
    moff_frac = moff/md
    okok = np.where(moff_frac < 100)[0]
    badbad = np.where(moff_frac >= 100)[0]
    sss = np.argsort(moff_frac[badbad])[::-1][0:30]
    mmm = np.ones(len(rawdata.t),dtype=int)
    mmm[badbad[sss]] = 0
    okok = np.where((mmm == 1) & (np.isnan(rawdata.t) == False))[0]
    print 'removed '  + str(len(rawdata.t)-len(okok)) + ' points right off the bat'
    rawdata = rawdata[okok]
    newxc = newxc[okok]
    newyc = newyc[okok]
    #import pdb
    #import matplotlib.pyplot as plt
    #plt.plot(rawdata.t,psffluxer[okok,qq],'.')
    #pdb.set_trace()
    okok,moff,moff_frac  = 0,0,0

    ##division information for different campaigns
    if cnum == 4:
        ndiv1   = 2 
        ndiv2   = 8 
        ndiv3   = 6 
        nbinsin = 15
        cad1     = [103854,104220]
        cad2     = [104222,105852]
        cad3     = [105854,107211]
        c2 = False
        nomover = True
        thirdpart = True
    if cnum == 2:
        ndiv1   = 4
        ndiv2   = 4
        nbinsin = 25
        cad1    = [95547,97611]
        cad2    = [97641,99339]
        c2 = True
        nomover = False
        thirdpart = False
    if cnum == 5:
        ndiv1   = 8
        ndiv2   = 8
        nbinsin = 15
        cad1 = [107598,109372]
        cad2 = [109374,111213]
        c2 = False
        nomover = True
        thirdpart = False
        
    if cnum == 13:
        ##for C13 need to clip some rawdata
        x2 = np.where((rawdata.cadenceno > 144727) | (rawdata.cadenceno < 144716))[0]
        rawdata = rawdata[x2]
        newxc = newxc[x2]
        newyc = newyc[x2]
        x2 = np.where((rawdata.cadenceno > 144655) | (rawdata.cadenceno < 144620))[0]
        rawdata = rawdata[x2]
        newxc = newxc[x2]
        newyc = newyc[x2]

        ndiv1 = 2 # 2
        ndiv2 = 7 # 8
        ndiv3 = 7 # 6 for normal campaigns
        nbinsin = 15
        cad1 = [140923,141355]
        cad2 = [141356,143084]
        cad3 = [143095,144821]
        thirdpart = True
        nomover=True
        c2=False
        
        
        
    #k2sff_log.write(idstring + ' Up k2sffcorrect'+ ' \n')
    #k2sff_log.flush()  
         
    a1,flag1 = k2sffcorrect(rawdata, ndivisions=ndiv1, newxc=newxc,newyc=newyc,startcadence=cad1[0],endcadence=cad1[1],nomoving=nomover,nbins=nbinsin,c2=c2,ndays=numdays,ndaysfirst=numdaysfirst,gradual=gradual,idstring=idstring,thelogfile=thelogfile)

    if thelogfile != None:
        thelogfile.write('done a1 \n')
        thelogfile.flush()
    
    if cnum == 4:
        a2,flag2 = k2sffcorrect(rawdata, ndivisions=ndiv2, newxc=newxc,newyc=newyc,startcadence=cad2[0],endcadence=cad2[1],nomoving=nomover,nbins=nbinsin,c2=c2,ndays=numdays,ndaysfirst=numdaysfirst,gradual=gradual,idstring=idstring,thelogfile=thelogfile)      
    else: a2,flag2 = k2sffcorrect(rawdata, ndivisions=ndiv2, newxc=newxc,newyc=newyc,startcadence=cad2[0],endcadence=cad2[1],nomoving=nomover,nbins=nbinsin,laststruct=a1,c2=c2,ndays=numdays,ndaysfirst=numdaysfirst,gradual=gradual,idstring=idstring,thelogfile=thelogfile)      
    a2.division += ndiv1

    if thelogfile != None:
        thelogfile.write('done a2 \n')
        thelogfile.flush()
    
    finflag = flag2 +'_'+ flag1
    if thirdpart == True: 
        a3,flag3 = k2sffcorrect(rawdata, ndivisions=ndiv3, newxc=newxc,newyc=newyc,startcadence=cad3[0],endcadence=cad3[1],nomoving=True,nbins=nbinsin,laststruct=a2,c2=c2,ndays=numdays,ndaysfirst=numdaysfirst,gradual=gradual,idstring=idstring,thelogfile=thelogfile)
        finflag = flag3 +'_'+ flag2
        a3.division += ndiv1+ndiv2      
        fina = numpy.lib.recfunctions.stack_arrays((a1,a2,a3),asrecarray=True,usemask=False)
    else: fina = numpy.lib.recfunctions.stack_arrays((a1,a2),asrecarray=True,usemask=False)
    
    if thelogfile != None:
        thelogfile.write('done a3 \n')
        thelogfile.flush()

    #k2sff_log.write(idstring + ' Up to post'+ ' \n')
    #k2sff_log.flush()
    #k2sff_log.close()
    #print np.nanmax(fina.division)
    x = np.where(fina.notmoving == 1)[0]
    cdpppsf      = np.zeros(len(fina.fpsfraw[0]))
    #psfmasksize  = np.zeros(len(fina.fpsfraw[0]))
    cdppcirc     = cdpppsf.copy()
    #circmasksize = cdpppsf.copy()
    fpsfcor  = np.squeeze(np.dstack(fina.fpsfcor))
    fpsfraw  = np.squeeze(np.dstack(fina.fpsfraw))
    fcirccor = np.squeeze(np.dstack(fina.fcirccor))

    for j in range(len(fina.fpsfraw[0])):
        cdpppsf[j] = cdpp(fpsfcor[j,x])
        #psfmasksize[j] = np.sum(psfmasks[j,:,:])
        cdppcirc[j] = cdpp(fcirccor[j,x])
        #circmasksize[j] = np.sum(circmasks[j,:,:])

    best = np.argmin(cdpppsf)
    best2= np.argmin(cdppcirc)
    usepsf =1
    if cdpppsf[best] > cdppcirc[best2]: usepsf=0
    
    

    ##skipping lots of plots here
    ##
    ##
    ##
    
    data = fina
    tcut = fina.t[x].copy()
    fcorcut = fpsfcor[best,x]
    frawcut = fpsfraw[best,x]
    
    ##force a certian aperture here by uncommenting this line
    ##fcorcut = fpsfcor[4,x]
    
    ##return the data, saving can be for a wrapper program if needed:
    dl = tcut.shape[0]    
    outdata         = np.recarray((dl,),dtype=[('t',float),('fraw',float),('fcor',float),('s',float),('qual',int),('detrend',float),('divisions',float)])
    outdata.t       = tcut
    outdata.fraw    = frawcut
    outdata.fcor    = fcorcut
    outdata.s       = data.arclength[x]
    outdata.qual    = data.moving[x]
    outdata.detrend = data.moving[x]*0.0
    outdata.divisions = data.division[x]*1.0 ##added this for later searching
    clean = True
    if clean == True:
        keep    = np.where(outdata.qual ==0)[0]
        outdata = outdata[keep]
    
    
    
    
    return outdata,data,tcut,fcorcut,frawcut,finflag
    
#@profile
def k2sffcorrect(rawdata,ndivisions=15,newxc=False,newyc=False,startcadence=103854,endcadence=104220,nomoving=False,nbins=25,ndays=1.5,ndaysfirst=1.5,c2=False,c1=True,c0=False,gradual=False,laststruct=None,yesplot=False,teststop=False,idstring='',thelogfile=None):

    #k2sffc_log = open('logfiles/k2sffclog.txt','ab')
   
    iterate  = True
    nitercor = 2
    
    ##testing exceptions handling here!!!
    ##dummy = undefined_variable
       
    allarclength = 1
    anchor       = False
    plotcentroids = 0
    usekepspl     = 0
    firstmedianfilter = False
    firstonediv       = True
    niters = 5
    plotall = False
    lookcirc = 0
    lookat = 5
    if ndivisions == 1: firstonediv = False
    if gradual == True: ndarray = np.linspace(ndaysfirst,ndays,niters+1)
    
    ##deprecated, now passing rawdata for speed
    ##now unpack loaddat into the correct variables
    #channelnum = loaddat['channelnum'].copy()
    #circmasks  = loaddat['circmasks'].copy()
    #flag       = loaddat['flag']
    flag= '0 ID' + idstring
    #modulenum  = loaddat['modulenum'].copy()
    #psfmasks   = loaddat['psfmasks'].copy()
    #rawdata    = loaddat['rawdata'].copy()
    #timage     = loaddat['timage'].copy()
    #xcenter    = loaddat['xcenter'].copy()
    #ycenter    = loaddat['ycenter'].copy()
    
    ##unse better centroid estimate if asked to
    if (newxc != False).any() & (newyc != False).any(): 
        rawdata['xcms'] = newxc.copy()
        rawdata['ycms'] = newyc.copy()
        rawdata['xcmfs'] = newxc.copy()
        rawdata['ycmfs'] = newyc.copy()

    oldrawdata = rawdata.copy()
    qnums = rawdata.quality.astype(int)

    ##Figure out which cadences have the correct conditions, this can vary by campaign number hence the bunch of ifs
    ##the default for most campaigns
    #x = np.where((rawdata.quality == 0) | (rawdata.quality == 16384) | (rawdata.quality == 2048) | (rawdata.quality == 8192))[0]
    
    #for C1, limit the minimun cadence number, this is the default now because it works for the above case also (c1 is true by default) in k2sffcorrect()
    if c1 == True: x = np.where(((rawdata.quality == 0) | (rawdata.quality == 2048) | (rawdata.quality == 8192)) & (rawdata.cadenceno >= (91433 + 220)))[0]

    ##for Campaign 2, include more qualities.
    if c2 == True: x = np.where((rawdata.quality == 0) | (rawdata.quality == 2048) | (rawdata.quality == 8192) | (rawdata.quality == 16384))[0]


    nmasklevels = len(rawdata[1].fcirc)
    npsflevels  = nmasklevels

    ##now get cadences in correct range
    x2 = np.where((rawdata.cadenceno[x] >= startcadence) & (rawdata.cadenceno[x] <= endcadence))[0]
    x  = x[x2]
    
    ##keep just the raw data in the given ranges
    rawdata = rawdata[x]
    
    ##this is a plotting thing, skipping
    ##clean nans/infs out of timage
    #badt = np.where(np.isfinite(timage) == False)
    #timage[badt[0],badt[1]] = 0.0
    #timage[badt[0],badt[1]] = np.nanmedian(timage)
    
    ##define a recarray for the output data
    #k2sffc_log.write(idstring + ' making arrays \n')
    #k2sffc_log.flush()
    data = np.recarray((len(rawdata),),dtype=[('t',float),('fcircraw','O'),('fpsfraw','O'),('fcirccor','O'),('fpsfcor','O'),('fforcorc','O'),('fforcorp','O'),
                       ('quality',float),('rays',float),('medians',float),('robmeans',float),('xcms',float),('ycms',float),('xcmfs',float),('ycmfs',float),('cadenceno',float),
                       ('xc',float),('yc',float),('xcold',float),('ycold',float),('xcfold',float),('ycfold',float),('division',int),('polyy',float),
                       ('arclength',float),('moving',int),('notmoving',int),('dsdt',float),('correction','O'),('correctionp','O'),('usedincor','O'),
                       ('usedincorp','O'),('lowfr','O'),('lowfrp','O')])
    ##fill the output structure for initialization
    data['t'] = rawdata.t.copy()
    data['fcircraw'] = rawdata.fcirc.copy()
    data['fpsfraw'] = rawdata.fpsf.copy()
    data['fcirccor'] = 0.0*rawdata.fcirc -1.0
    data['fpsfcor'] = 0.0*rawdata.fpsf -1.0
    data['fforcorc'] = 0.0*rawdata.fpsf -1.0
    data['fforcorp'] = 0.0*rawdata.fpsf -1.0
    data['quality'] = rawdata.quality.copy()
    data['rays'] = rawdata.rays.copy()
    data['medians'] = rawdata.medians.copy()
    data['robmeans'] = rawdata.robmeans.copy()
    data['xcms'] = rawdata.xcms.copy()
    data['ycms'] = rawdata.ycms.copy()
    data['xcmfs'] = rawdata.xcmfs.copy()
    data['ycmfs'] = rawdata.ycmfs.copy()
    data['cadenceno'] = rawdata.cadenceno.copy()
    data['xc'] = -1.
    data['yc'] = -1.
    data['ycold'] = -1.0
    data['ycfold'] = -1.0
    data['xcold'] = -1.0
    data['xcfold'] = -1.0
    data['polyy'] = -1.0
    data['arclength']  = -1.0
    data['moving'] = 0
    data['notmoving'] = 1
    data['dsdt'] = -1
    data['correction'] = -1.0*rawdata.fcirc
    data['correctionp'] = -1.0*rawdata.fcirc
    data['usedincor'] = 0.0*rawdata.fcirc
    data['usedincorp'] = 0.0*rawdata.fcirc
    data['lowfr'] = -1.0*rawdata.fcirc
    data['lowfrp'] = -1.0*rawdata.fcirc
    ##define the divisions, making sure everything is in at least one division
    data['division']      = np.ceil(ndivisions*(data.t-min(data.t))/(np.nanmax(data.t)-np.nanmin(data.t))) -1 
    qwe                   = np.where(data.division < 0)[0]
    data.division[qwe]    = 0

    

    ##convert pixels to arcseconds
    xc  = (data.xcms - np.nanmedian(data.xcms))*3.98
    yc  = (data.ycms - np.nanmedian(data.ycms))*3.98
    xcf = (data.xcmfs- np.nanmedian(data.xcmfs))*3.98
    ycf = (data.ycmfs- np.nanmedian(data.ycmfs))*3.98

    A = np.array([xc,yc])
    #k2sffc_log.write(idstring + ' Matrix A check'+ ' \n')
    #k2sffc_log.flush()
    ##make sure A isnt messed up
    if len(np.where(np.isfinite(A))[0]) == 0:
        print idstring + ' BOMB -- All of the data are nan' 
        flag += ' Failed A matrix check '
        return data,flag
        
    cov             = np.cov(A)
    eigenvals,evecs = np.linalg.eig(cov)
    order           = np.argsort(np.absolute(eigenvals))[::-1]
    if order[0] == 1: 
        evecs = transpose(evecs)
        eigenvals       = eigenvals[order]
    theta = np.arcsin(evecs[0,1])
    arot  = -np.dot(np.transpose(A),evecs) ##negative needed because IDL's eigenvectors are left handed I think this shouldn't actually matter mathematically as far as I can tell
    data['xcold'] = xc
    data['ycold'] = yc
    xc         = arot[:,0]
    yc         = arot[:,1]
    
    B =  np.array([xcf,ycf])
    covb             = np.cov(B)
    eigenvalsb,evecsb = np.linalg.eig(covb)
    order           = np.argsort(np.absolute(eigenvalsb))[::-1]
    if order[0] == 1: 
        evecsb = transpose(evecsb)
        eigenvalsb       = eigenvalsb[order]
    brot  = -np.dot(np.transpose(B),evecsb)
    data['xcfold'] = xcf
    data['ycfold'] = ycf
    xcf         = brot[:,0]
    ycf         = brot[:,1]

    firstgroup    = np.where(data.division == 0)[0]
    goodcentroids = outlierbinrej(xc[firstgroup],yc[firstgroup],10,5)
    coeffs        = np.polyfit(xc[firstgroup[goodcentroids]],yc[firstgroup[goodcentroids]],5)
    polyy         = np.polyval(coeffs,xc[firstgroup])
    
    
    goodcentroidsf = outlierbinrej(xcf[firstgroup],ycf[firstgroup],10,5)
    coeffsf        = np.polyfit(xcf[firstgroup[goodcentroids]],ycf[firstgroup[goodcentroids]],5)
    polyyf         = np.polyval(coeffs,xcf[firstgroup])

    data['xc'] = xc.copy()
    data['yc'] = yc.copy()
    
    if np.std(polyyf - ycf[firstgroup]) < np.std(polyy - yc[firstgroup]):
        print 'Switching to fit centroids'
        flag   += ' Fit Centroids'
        data['xc'] = xcf
        data['yc'] =  ycf
        
    divsize = -1
    ndivs   = np.nanmax(data.division)+1
    for i in range(np.nanmax(data.division)+1):
        nthisdiv = len(np.where(data.division == i)[0])
        if nthisdiv > divsize: divsize = nthisdiv
    
    if allarclength == 1: 
        goodcentroids = outlierbinrej(data.xc,data.yc,10,5)
        coeffs        = np.polyfit(data.xc[goodcentroids],data.yc[goodcentroids],5)
        data['polyy']    = np.polyval(coeffs, data.xc)
        sor = np.argsort(data.xc)
        data.arclength[sor] = inttrap(data.xc[sor],np.sqrt(1+np.polyval(np.polyder(coeffs),data.xc[sor])**2),cumulative=True) 

        data.dsdt                 = diff(data.arclength)/diff(data.t)
        ##identical here:

        m,jsigma,jnum_rej,goodindal = robustmean(data.dsdt-keplerspline(data.t,data.dsdt),5)

        moving = np.setdiff1d(np.arange(len(data)),goodindal)
        
        if len(moving) != len(data): data.moving[moving] = 1
        data.notmoving = 1-data.moving
        
            
    ##for the case where we known the moving points are pre-removed:    
    if nomoving == True:    
        data.notmoving[:] = 1
        data.moving[:]    = 0
    
    notmoving = np.where(data.notmoving == 1)[0]
    moving    = np.where(data.moving == 1)[0]
    
    if thelogfile != None:
        thelogfile.write('Done with Matrix A \n')
        thelogfile.flush()
    
    ##!!!Do the circular apertures first:
    anchors = np.nanmean(data.arclength) #new in rev 5 of C0, but not used
    anchorf = 0
    #k2sffc_log.write(idstring + ' running onediv'+ ' \n')
    #k2sffc_log.flush()
    if ndivisions >1:   
        onedivdata,dummyflag = k2sffcorrect(oldrawdata,ndivisions=1,newxc=newxc,newyc=newyc,ndays=ndays,c0=c0,c2=c2,c1=c1,startcadence=startcadence,endcadence=endcadence,teststop=True,idstring=idstring)
        odfcirccor = np.squeeze(np.dstack(onedivdata.fcirccor)).copy()
        odfpsfcor  = np.squeeze(np.dstack(onedivdata.fpsfcor)).copy()
        if thelogfile != None:
            thelogfile.write('Done with OneDiv Reduction \n')
            thelogfile.flush()
        
    ##dereference the recarray structure for easy manipulation
    fcircraw   = np.squeeze(np.dstack(data.fcircraw))
    fforcorc   = np.squeeze(np.dstack(data.fforcorc))
    lowfr      = np.squeeze(np.dstack(data.lowfr))
    usedincor  = np.squeeze(np.dstack(data.usedincor))
    correction = np.squeeze(np.dstack(data.correction))
    fcirccor    = np.squeeze(np.dstack(data.fcirccor))

    if type(laststruct) != type(None):
        lastlowfr    = np.squeeze(np.dstack(laststruct.lowfr))
        lastlowfrp   = np.squeeze(np.dstack(laststruct.lowfrp))

    #k2sffc_log.write(idstring + ' circ apertures' + ' \n')
    #k2sffc_log.flush()
    for i in range(nmasklevels):
        if thelogfile != None:
            thelogfile.write('Doing circular mask ' + str(i) + ' \n')
            thelogfile.flush()
        for iters in range(niters+1):
            fforcorc[i] = fcircraw[i].copy()
            #k2sffc_log.write(idstring + ' in in aperture C' + str(i) + '\n')
            #k2sffc_log.flush()
            if np.sum(1-np.isfinite(fcircraw[i])) !=0:
                print idstring +' BOMB -- the raw data is not finite somewhere -- returning as-is.'
                flag += 'BOMB -- the raw data is not finite somewhere -- returning as-is.'
                return data
                
            
        
            if thelogfile != None:
                thelogfile.write('Doing Keplerslpine \n')
                thelogfile.flush()
            if gradual == False:  # make this block in if-statement for the case when gradual adjustment to short timescale spline is not implemented
                if firstmedianfilter == True : lowpassed = keplerspline(data.t,medianfilter(fcircraw[i],300,reflect=True),ndays=ndaysfirst)
                if firstmedianfilter == False: lowpassed= keplerspline(data.t,fcircraw[i],ndays=ndaysfirst,thisstop=False)
                if (firstmedianfilter == False) & (firstonediv == True): lowpassed = keplerspline(onedivdata.t,odfcirccor[i],ndays=ndaysfirst)
                if iters > 0:
                    lowpassed = keplerspline(data.t,fcirccor[i],ndays=ndays)
                    #if np.sum(lowpassed == np.zeros(len(lowpassed),dtype=float)) == len(lowpassed): lowpassed +=1
                    if np.sum(lowpassed == 0.0) == len(lowpassed): lowpassed +=1
            if gradual == True:
                if iters == 0:
                    if firstonediv == False: lowpassed = keplerspline(data.t,fcircraw[i],ndays=ndaysfirst)
                    if firstonediv == True : lowpassed = keplerspline(onedivdata.t,odfcirccor[i],ndays=ndaysfirst)
                    
                if iters > 0:    
                    lowpassed = keplerspline(data.t,fcirccor[i],ndays=ndarray[iters])
                    if np.sum(lowpassed == 0.0) == len(lowpassed): lowpassed +=1
                            

          
            ##!!
            if type(laststruct) != type(None): lowpassed = lowpassed*lastlowfr[i,len(laststruct)-1]/lowpassed[0]
            
            lowfr[i] = lowpassed
            
            
            
            ##skipping plot statement region here
            if yesplot == True: 'Not Plotting Mate!'
           
            fforcorc[i] /= lowpassed #added for rev 22-23

            
            if thelogfile != None:
                thelogfile.write('Doing acrlenght fit  \n')
                thelogfile.flush()
            for j in range(ndivs):
                thisdiv       = np.where(data.division  == j)[0]
                thismoving    = np.where(data.moving[thisdiv] == 1)[0]
                thisnotmoving = np.where(data.notmoving[thisdiv] == 1)[0]
                if plotall == True: print 'Not plotting Mate'
                
                goodinds = thisnotmoving[outlierbinrej(data.arclength[thisdiv[thisnotmoving]],fforcorc[i,thisdiv[thisnotmoving]],15,3,dostop=False,thelogfile=None)]
                if thelogfile != None:
                    thelogfile.write('done goodinds  \n')
                    thelogfile.flush()
                
                
                if plotall == True: print 'Not Plotting Mate'
                usedincor[i,thisdiv[goodinds]] = 1
               
                if plotall == True: print 'Not plotting Mate'
            
                if (np.sum(1-np.isfinite(fforcorc[i,thisdiv[goodinds]])) != 0) | (len(goodinds) == 0):
                    print idstring + ' BOMB -- the data is not finite -- returning as-is.'
                    flag += 'SFF correction: BOMB -- the data is not all finite -- returning as-is.'
                    return data,flag
                    
                if thelogfile != None:
                    thelogfile.write('Doing correction polyfit \n')
                    thelogfile.flush()
                correctionfit = np.polyval(np.polyfit(data.arclength[thisdiv[goodinds]],fforcorc[i,thisdiv[goodinds]],5),data.arclength[thisdiv])
                
                if thelogfile != None:
                    thelogfile.write('Doing cfitbin fit \n')
                    thelogfile.flush()
                cfitbin,jbinsize,jebin,junk,albin = lcbin(data.arclength[thisdiv[goodinds]],fforcorc[i,thisdiv[goodinds]],nbins,userobustmean=True)
                haspoints = np.where(np.isfinite(cfitbin))[0]
                albin     = albin[haspoints]
                cfitbin   = cfitbin[haspoints]
                if len(haspoints) > nbins/2:
                    if thelogfile != None:
                        thelogfile.write('Doing interp fit \n')
                        thelogfile.flush()
                    correctionfit = np.interp(data.arclength[thisdiv],albin,cfitbin)

                    
                  
                    if iterate == True:
                        totalcorrection = correctionfit.copy()
                        for itercor in range(nitercor):
                            if thelogfile != None:
                                thelogfile.write('Doing iteration ' +str(itercor) + ' \n')
                                thelogfile.flush()
                            cfitbin,j1,j2,allgoodind,albin = lcbin(data.arclength[thisdiv],fforcorc[i,thisdiv]/totalcorrection,nbins,userobustmean=True)

                            ihaspoints = np.where(np.isfinite(cfitbin))[0]
                            albin      = albin[ihaspoints]
                            cfitbin    = cfitbin[ihaspoints]

                            if len(haspoints) > nbins/2:
                                thiscorrectionfit = np.interp(data.arclength[thisdiv],albin,cfitbin)    

                               # ff = scinterp.interp1d(albin,cfitbin,bounds_error=False,fill_value="extrapolate")
                               # thiscorrectionfit = ff(data.arclength[thisdiv])
                                totalcorrection  *= thiscorrectionfit
                                usedincor[i,thisdiv] = 0
                                usedincor[i,thisdiv[allgoodind]] = 1
                                #print 'iterating'
                    

                        correctionfit = totalcorrection.copy()
                
                if plotall == True: print 'Not Plotting Mate'
                if len(haspoints) <= nbins/2: flag += 'Reverting to Polynomial correction fit' 
            
                if thelogfile != None:
                    thelogfile.write('up to anchor \n')
                    thelogfile.flush()
                if anchor == True:
                    if j == 0: anchorf = np.interp(anchors,data.arclength[thisdiv],correctionfit)
                    if j >= 0:
                        print 'Anchor is ',anchorf
                        dummy = np.interp(anchors,data.arclength[thisdiv],correctionfit)
                        print 'correction at anchor is ', dummy
                        correctionfit = correctionfit*anchorf/dummy
                        print 'correction at anchor is now',np.interp(anchors,data.arclength[thisdiv],correctionfit)

                ##if len(thisdiv) != len(correctionfit): pdb.set_trace()

                correction[i,thisdiv] = correctionfit.copy()
                
                ##skipping a plot statement here:
                
            fcirccor[i] = fcircraw[i]/correction[i]




    ##circular apertures done re-reference the important parts:
    for jj in range(fcirccor.shape[1]):
        data.fcirccor[jj] = fcirccor[:,jj].copy()
        data.fforcorc[jj] = fforcorc[:,jj].copy()
        data.lowfr[jj]    = lowfr[:,jj].copy()
        data.correction[jj] = correction[:,jj].copy()
        data.usedincor[jj]  = usedincor[:,jj].copy()
    
    ##now do the PSF apertures!
    
    ##dereference the corresponding things for the PSFs
    fpsfraw     = np.squeeze(np.dstack(data.fpsfraw))
    fpsfcor     = np.squeeze(np.dstack(data.fpsfcor))
    lowfrp      = np.squeeze(np.dstack(data.lowfrp))
    usedincorp  = np.squeeze(np.dstack(data.usedincorp))
    correctionp = np.squeeze(np.dstack(data.correctionp))
    fforcorp    = np.squeeze(np.dstack(data.fforcorp))

    for i in range(npsflevels):
        if thelogfile != None:
            thelogfile.write('Doing PSF mask ' + str(i) + ' \n')
            thelogfile.flush()
        for iters in range(niters+1):
            fforcorp[i] = fpsfraw[i].copy()
            if np.sum(1-np.isfinite(fpsfraw[i])) != 0:
                print  idstring +' BOMB -- the raw data is not finite somewhere -- returning as-is.'
                flag+= 'BOMB -- the raw data is not finite somewhere -- returning as-is.'
                return data,flag
        
            if thelogfile != None:
                thelogfile.write('Doing Keplersline \n')
                thelogfile.flush()
            if gradual == False: 
                if firstmedianfilter == True : lowpassed = keplerspline(data.t,medianfilter(fpsfraw[i],300,reflect=True),ndays=ndaysfirst)
                if firstmedianfilter == False: lowpassed = keplerspline(data.t,fpsfraw[i],ndays=ndaysfirst)
                if (firstmedianfilter == False) & (firstonediv == True): lowpassed = keplerspline(onedivdata.t,odfpsfcor[i],ndays=ndaysfirst)
                
                if iters > 0:
                    lowpassed = keplerspline(data.t,fpsfcor[i],ndays=ndays)
                    if np.sum(lowpassed == 0.0) == len(lowpassed): lowpassed +=1
                
                
            ##gradual adjustment here
            if gradual == True:
                if iters == 0:
                    if firstonediv == False: lowpassed = keplerspline(data.t,fpsfraw[i],ndays=ndaysfirst)
                    if firstonediv == True : lowpassed = keplerspline(onedivdata.t,odfpsfcor[i],ndays=ndaysfirst)
                if iters > 0:
                    lowpassed = keplerspline(data.t,fpsfcor[i],ndays=ndarray[iters])
                    if np.sum(lowpassed == 0.0) == len(lowpassed): lowpassed +=1
            
            ##end gradual adjust here
            
            if type(laststruct) != type(None): lowpassed = lowpassed*lastlowfrp[i,len(laststruct)-1]/lowpassed[0]
            
            lowfrp[i] = lowpassed
            
            ##skipping plot statements here
            ##
            ##
            
            fforcorp[i] /= lowpassed
            
            if thelogfile != None:
                thelogfile.write('Doing arclength fit \n')
                thelogfile.flush()
            for j in range(ndivs):
                thisdiv       = np.where(data.division == j)[0]
                thismoving    = np.where(data.moving[thisdiv] == 1)[0]
                thisnotmoving = np.where(data.notmoving[thisdiv] ==1)[0]            
                if plotall == True : print 'not plotting mate!'
                

                goodinds = thisnotmoving[outlierbinrej(data.arclength[thisdiv[thisnotmoving]],fforcorp[i,thisdiv[thisnotmoving]],15,3,dostop=False,thelogfile=None)]
                
                if plotall == True: print 'not plotting mate'
                usedincorp[i,thisdiv[goodinds]] = 1
                
                
                if np.sum((1-np.isfinite(fforcorp[i,thisdiv[goodinds]])) != 0) | (np.sum(1-np.isfinite(fforcorc[i,thisdiv[goodinds]])) != 0) | (len(goodinds) == 0):
                    print  idstring +' BOMB -- the data is not finite -- returning as-is.'
                    flag+= 'SFF correction: BOMB -- the data is not finite -- returning as-is.'
                    return data,flag
                
                
                correctionfit = np.polyval(np.polyfit(data.arclength[thisdiv[goodinds]],fforcorp[i,thisdiv[goodinds]],5),data.arclength[thisdiv])

                cfitbin,jbinsize,jebin,junk,albin = lcbin(data.arclength[thisdiv[goodinds]],fforcorp[i,thisdiv[goodinds]],nbins,userobustmean=True)
                
                haspoints = np.where(np.isfinite(cfitbin))[0]
                albin     = albin[haspoints]
                cfitbin   = cfitbin[haspoints]
                if len(haspoints) > nbins/2:
                    correctionfit = np.interp(data.arclength[thisdiv],albin,cfitbin)
#                    ff = scinterp.interp1d(albin,cfitbin,bounds_error=False,fill_value="extrapolate")
#                    correctionfit = ff(data.arclength[thisdiv])
                    if iterate == True:
                        totalcorrection = correctionfit.copy()
                        for itercor in range(nitercor):
                            cfitbin,j1,j2,allgoodind,albin = lcbin(data.arclength[thisdiv],fforcorp[i,thisdiv]/totalcorrection,nbins,userobustmean=True)
                            ihaspoints = np.where(np.isfinite(cfitbin))[0]
                            albin      = albin[ihaspoints]
                            cfitbin    = cfitbin[ihaspoints]
                            if len(haspoints) > nbins/2:
                                thiscorrectionfit = np.interp(data.arclength[thisdiv],albin,cfitbin)
                             #   ff = scinterp.interp1d(albin,cfitbin,bounds_error=False,fill_value="extrapolate")
                            #    thiscorrectionfit = ff(data.arclength[thisdiv])
                                totalcorrection *= thiscorrectionfit
                                usedincorp[i,thisdiv] = 0
                                usedincorp[i,thisdiv[allgoodind]] = 1
                                #print 'iterating'
                        correctionfit = totalcorrection.copy()
                    
                if len(haspoints) <= nbins/2: flag += 'Reverting to polynomial correction fit'
                correctionp[i,thisdiv] = correctionfit.copy()
                
            fpsfcor[i] = fpsfraw[i]/correctionp[i]
    
    
    #k2sffc_log.write(idstring + ' made it to end')
    #k2sffc_log.flush()
    #k2sffc_log.close()

    ##PSF apertures done re-reference the important parts:
    for jj in range(fcirccor.shape[1]):
        data.fpsfcor[jj] = fpsfcor[:,jj].copy()
        data.fforcorp[jj] = fforcorp[:,jj].copy()
        data.lowfrp[jj]    = lowfrp[:,jj].copy()
        data.correctionp[jj] = correctionp[:,jj].copy()
        data.usedincorp[jj]  = usedincorp[:,jj].copy()

    #finish and output
    return data,flag

##


# 
# epic = 210317378
# file = 'k2data/'+'rawrev/ktwo'+str(epic)+'extr.idl'
# stuff=idlread(file,python_dict=True)
# psfmasks = stuff['psfmasks']
# circmasks= stuff['circmasks']
# rawdata  = stuff['rawdata']
# 
# 
# pdb.set_trace()
# 
# readydata,data,tcut,fcorcut,frawcut,flag = reducek2c4_rawrev2rev(rawdata,epic)
# 
# 
# 
# pdb.set_trace()
# print 'Im Here'

#reducek2c4,epic = 210317378   









