##standard imports
import numpy as np
import pickle
import scipy.stats
from scipy.io.idl import readsav as idlread
import os,warnings,signal
from scipy.optimize import curve_fit

##installed things
import batman
import bls
import mpyfit ##using mpyfit for the sliding window because its a C version and is incredibly fast compared to the python only version. Results are identical.

##my imports for future tracking
##These are all in pyutils
#from mpfit   import mpfit not used anymore
from gaussfit import gaussfit
from gaussfit import gaussfit_mp
import k2sff



np.set_printoptions(suppress=True)

##this file contains all the functions for the k2 detrending and injection-recovery testing
##it only contains function, so do
##import detrent_k2
##then to run things to .function()
##does not include k2sff (thats in k2sff.py)
    
def timeout_handler(signum,frame):
    '''
    handler for use with signal to catch timeouts
    '''
    raise Exception('timeout error')

##a function that bins a lightcurve into a certain number of bins
def binlc(x,y,nbins=100):
    binedges = (np.max(x)-np.min(x))/nbins*np.linspace(0,nbins,nbins+1)+np.min(x)
    binsize  = binedges[1]-binedges[0]
    xbin     = binedges[1:]-binsize/2.0
    meds     = np.zeros(nbins)
    for i in range(nbins):
        inthis  = np.where((x >= binedges[i]) & (x < binedges[i+1]))[0]
        meds[i] = np.nanmedian(y[inthis])
        
    return xbin,meds

##function to calculate phases given a period and a starting time for the oscillation
def calc_phase(time,period,p0):
    anchor = np.floor((time-p0)/period)
    phase = (time-p0)/period - anchor
    return phase

##plots light curve (time vs flux), with vertical bars marking the indicies provided. Mainly for debugging
def markplot(time,flux,indices):
    plt.plot(time,flux,'b')
    plt.plot(time,flux,'.b')
    bad   = np.isnan(flux)
    good  = [not i for i in bad]
    clean = np.where(good)[0]
    for i in range(len(indices)):
        plt.plot([time[indices[i]],time[indices[i]]],[np.min(flux[clean]),np.max(flux[clean])],'r--')
    plt.show()
    #return True

##function that reads Andrew Vanderburgs light curve files from MAST
##everything is already cleaned in Andrews files, so things are easy
def read_datafile_vanderburg(filename,clean=False):
    from astropy.io import fits
    hdulist    = fits.open(filename)
    head       = hdulist[1].header
    data       = hdulist[1].data
    dl         = data.T.shape[0]
    ##extract relevant parameters into our own recarray with easier to use tagnames
    outdata         = np.recarray((dl,),dtype=[('t',float),('fraw',float),('fcor',float),('s',float),('qual',int),('detrend',float),('divisions',int)])
    outdata.t       = data.T
    outdata.fraw    = data.FRAW
    outdata.fcor    = data.FCOR
    outdata.s       = data.ARCLENGTH
    outdata.qual    = data.MOVING
    outdata.detrend = data.MOVING*0.0
    
    ##make up the divisions:
    ndivisions=25
    newdiv = np.ceil(ndivisions*(outdata.t-np.nanmin(outdata.t))/(np.nanmax(outdata.t)-np.nanmin(outdata.t))) -1 
    qwe                   = np.where(newdiv < 0)[0]
    newdiv[qwe]    = 0
    outdata.divisions = newdiv
    

    ##for campaigns < 3, need to clean away thruster fire points.
    ##doing it for pre-cleaned data won't cause issues.
    if clean == True:
        thrusterok = np.where(outdata.qual == 0)[0]
        outdata    = outdata[thrusterok]
    
    return outdata


##function that reads output files from AndrewV's pipeline and converts them into the standard 
##recarrays used in the detrending pipeline
def read_datafile_avextract(filename,clean=False): 
    stuff  = idlread(filename)
    data   = stuff.data.T ##not sure why T is needed here.....
    cln    = np.where(data.notmoving == 1)[0]
    bestfcor = stuff.fcorcut
    time   = stuff.tcut
    psfcor = np.array(data.FPSFCOR.tolist())
    ccor   = np.array(data.FCIRCCOR.tolist())
    psfraw = np.array(data.FPSFRAW.tolist())
    ##figure out which psf was the best
    block  = np.tile(bestfcor,(10,1)).T
    test   = np.sum(psfcor[cln,:]-block,axis=0)
    qwe    = np.where(test == 0)[0]
    if len(qwe) != 0:
        bestraw = psfraw[cln,qwe[0]]
        aptype = 'PSF'
    else: 
        ccor   = np.array(data.FCIRCCOR.tolist())
        rcor   = np.array(data.FCIRCRAW.tolist())
        test   = np.sum(ccor[cln,:]-block,axis=0)
        qwe    = np.wher(test == 0)[0]
        if len(qwe) == 0:
            print 'Cant find best aperture'
            import pdb
            pdb.set_trace()
        bestraw = rcor[cln,qwe[0]]
        aptype = 'CIRC'
    
    dl = time.shape[0]    
    outdata         = np.recarray((dl,),dtype=[('t',float),('fraw',float),('fcor',float),('s',float),('qual',int),('detrend',float)])
    outdata.t       = time
    outdata.fraw    = bestraw
    outdata.fcor    = bestfcor
    outdata.s       = data.ARCLENGTH[cln]
    outdata.qual    = data.MOVING[cln]
    outdata.detrend = data.MOVING[cln]*0.0

    
    if clean == True:
        keep    = np.where(outdata.qual ==0)[0]
        outdata = outdata[keep]

    return outdata,qwe[0],aptype
    

##transit like box function. Depth is a fractional depth, width is in days (as is x)
def notchbox(x,depth,width,middle):
    tbox         = x.copy()*0.0 +1
    inside       = np.where(np.absolute(x-middle) < width/2)[0]
    tbox[inside] = 1.0-depth
    return tbox


def transit_window_slide(p,t=None,fl=None,sig_fl=None,s=None,ttime=None,fjac=None,model=False):
    ##if called by mpfit, one-line it all for speed 
    #if model == False: return [0,(fl-(p[0] + p[1]*t + p[2]*t**2 + p[3]*s + p[4]*s**2 + p[7]*s**3)*notchbox(t,p[5],p[6],ttime))/sig_fl]
    ##if user wants all the things, do it step by step
    #if model == True : 
    polyshape = p[0] + p[1]*t + p[2]*t**2 + p[3]*s + p[4]*s**2 + p[7]*s**3
        ##notch box
    transitshape = notchbox(t,p[5],p[6],ttime) 
        ##final model
    themod = polyshape*transitshape
        ##leastsq residuals
    resid = (fl-themod)/sig_fl
    if model == True: return themod,polyshape,transitshape,resid
    return [0,resid]

##transit window function for mpyfit usage
def transit_window_slide_pyfit(p,args,model=False):
    ##unpack the arguaments
    t,fl,sig_fl,s,ttime = args
    polyshape = p[0] + p[1]*t + p[2]*t**2 + p[3]*s + p[4]*s**2 + p[7]*s**3
    transitshape = notchbox(t,p[5],p[6],ttime) 
    themod = polyshape*transitshape
    resid = (fl-themod)/sig_fl
    if model == True: return themod,polyshape,transitshape,resid
    return resid
    
    
def sliding_window(data,windowsize=0.5, use_arclength=False, use_raw=False,efrac=1e-3,resolvable_trans=False,cleanmask=[-1,-1],deltabic=-1.0):
    '''
    Sliding Window Notch-Filter
    
    THis code takes a lightcurve and applies the notch-filter method to remove rotation and preserve trasits. 

    Inputs:
    (1) data: The data recarray that this code uses for passing data around
    
    Optional Inputs:
    (1) windowsize: Detrending window size in days, default is 0.5 days.
    (2) use_arclength: Set to True to do a full fit over time and arclength, default is False
    (3) use_raw: Set to True to use raw data that is uncorrected for K2 pointing systematics. Default is False
    (4) efrac: starting fractional uncertainty on the lightcurve data. Default is 1mmag. This value is dynamically determined in each fitting window
    and so should only be change if extreme circumstances.
    (4) resolvable_trans: Set to not use the 45 min transit window trail. Default is False.
    (5) cleanmask: binary mask to remove a set of points from the fitting. Great for masking over a transit signal that you 
    dont want influencing the fit. Default is [-1,-1] which turns it off 
    (6) detlabic: Bayesian information cirterion difference between the transit and no-transit model required to select the transit model. A higher value
    indicates more required evidence. Default is -1.0, which is at least equal evidence with a ~1 margin for uncertainty. Set to np.inf to always choose the null model
    or -np.inf to always choose the transit model.
        
    Outputs:
    (1) Times: The input time axis from data
    (2) depths: notch filter depths at each point in the lightcurve. zero when null model chosen
    (3) detrend: detrended lightcurve
    (4) polyshape: the model used to detrend the input lightcurve
    (5) badflag: integer flags for each datapoint in data with 0=fine 
        1=masked as outlier in iterations at least once but still fine, 2=strong positive outlier or other suspect point.
    '''

    
    
    
    
    import time
    wsize  = windowsize
    wnum   = windowsize/(data.t[10]-data.t[9])
    fittimes = data.t
    depthstore = fittimes.copy()*0.0
    detrend    = data.fraw*0.0
    polyshape  = data.fraw*0.0
    badflag    = np.zeros(len(data.fraw),int)
    badflag2   = badflag.copy()
    running_std_depth = 0.0
    cliplim = 3.0
    if cleanmask[0] != -1: cliplim=4.5
    ##sliding storage for detrending shape not using for now
    #slidestore = np.zeros((len(data.t),wnum*2))*np.nan
    #storepos   = np.zeros(len(data.t),int)
    for i in range(0,len(fittimes)):     
        ##an upper sco transit point 3000:
        #if np.mod(i+1,100) == 0: print 'Up to ' + str(i+1) + ' out of ' + str(len(fittimes)) + ' times'
        
        ##grab the window
        wind     = np.where((data.t < fittimes[i]+wsize/2.0) & (data.t > fittimes[i]-wsize/2.0))[0]
        wdat     = data[wind].copy()
        starttime = wdat.t[0]*1.0
        thistime = fittimes[i]-wdat.t[0]
        wdat.t  -= wdat.t[0]
        ttt      = np.where(wdat.t == thistime)[0]
        if cleanmask[0] != -1: wcleanmask = cleanmask[wind].copy()
    
        ##switch out the raw flux for the Vanderburg flat-fielded flux if not using arc-length parameters
        if use_raw == False: wdat.fraw = wdat.fcor
        
        
        ##now linearize the data for initial checking:
        ##and impose a threshold cut for flaring events
        line      = np.polyval(np.polyfit(wdat.t,wdat.fraw,1),wdat.t)
        lineoff   = np.sqrt(np.nanmean((wdat.fraw-line)**2))
        lineresid = (wdat.fraw-line)/lineoff
        #mlevel = np.median(wdat.fraw)
        #if use_raw == False: flare  = np.where((wdat.fraw/mlevel > 1.005) | (lineresid > 3.0))[0]
        #if use_raw == True:  flare  = np.where(lineresid > 3.0)[0]
        flare  = np.where((lineresid > 8.0) | (wdat.fraw < 0.0))[0]
        wdat.qual[flare] = 2
        #extrabad = np.where(wdat.fraw < 0.0)[0]
        #flar
        
        ##fit the model to the window at this transit time
        ##build the inputs for MPYFIT
        pstart  = np.array([np.nanmedian(wdat.fraw),-0.01,-0.01,0.0,0.0,0.002,0.08,0.0])
        ##run a polyfit to get starting parameters for a full fit:
        initpoly = np.polyfit(wdat.t,wdat.fraw,2)
        pstart[0] = initpoly[2]
        pstart[1]  = initpoly[1]
        pstart[2] = initpoly[0]
        error   = efrac*wdat.fraw
        ##find zero points, this happens on rare occasions and messes things up badly if not dealt with
        qwe = np.where(wdat.fraw == 0.0)[0]
        error[qwe] = np.inf ##assign it a dramatic error
        
        ##find a transit in the first third of the window that has already been passed
        ##this is actually not a good idea: at best a duplication of the outlier steps, at worst, 
        ##eats points that should be included in fit
        #passed_trans = np.where((detrend[wind] < 0.999) & (wind-wind[0] < len(wind)/3))[0]
        ##what we really should do is keep a running depth standard deviation
        passed_trans = np.where((depthstore[wind] >=5.0*np.std(depthstore[0:i+1])) & (wind-wind[0] < len(wind)/3))[0]
        if i < 10: passed_trans = np.where(depthstore[wind] > 1e10)[0]
        
        ##has something previously been flagged as bad? This should only be points ID'd as flares in passed windows
        ##this step basically is an ensurance policy for when multiple flares creep into a window making clipping hard.
        suspect = np.where((badflag[wind] ==2) & (wind != ttt))[0]
        
        ##combine the flare points, suspect points, and passed_transits
        dontuse = np.unique(np.append(np.append(flare,suspect),passed_trans))
    
        ##if running the cleanup detrending when we known were the transit is:
        ##this should mean all in-transit points don't alter the detrending fit
        if cleanmask[0] != -1:
            intrans = np.where(wcleanmask == 99)[0]
            dontuse = np.unique(np.append(dontuse,intrans))
        
        ##set all these points to bad
        error[dontuse] = np.inf
        
        ##set up the fitting parinfo dictionary      
        parinfo2 = [{'fixed':False, 'limits':(None,None),'step':0.1} for dddd in range(pstart.shape[0])]
        parinfo2[5]['step']      = 1.0
        parinfo2[6]['step']      = 0.0105
        parinfo2[5]['limits']    = (0.0,1.0)
        parinfo2[6]['limits']    = (0.02,0.2)
        
        ##if arc length is not going to be used in the fit, fix those parameters
        if use_arclength == False:
            parinfo2[3]['fixed'] = True
            parinfo2[4]['fixed'] = True
            parinfo2[7]['fixed'] = True
            
        if (use_raw == False) & (use_arclength == True): parinfo2[4]['fixed'] = True #make arclength fit linear when using corrected curves and arclength

        ##run on a grid of transit durations for the notch, for things that are planet like in depth
        lgrid = np.array([0.75,1.0,2.0,4.0])/24.0
        numpars = len(parinfo2) - np.sum([parinfo2[dude]['fixed'] for dude in range(len(parinfo2))])
        
        if len(wdat) < numpars+1+5:##if hardly any points in window, just use the initial fit 
            modpoly = np.polyval(initpoly,wdat.t)
            pos_outlier = np.array([],dtype=int)
            pars = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
            #print 'in nodata fit'
        else: ##if enough points in this window to do the full notch filter fit, go fit it
            #print 'in full fit'
            
            
            ##if switch to set, don't try the single point transit (45 mins)
            ##also adjust related fit limit
            if resolvable_trans == True: 
                lgrid = np.array([1.0,2.0,4.0])/24.0
                parinfo2[6]['limits'] = (0.042,0.2)        
            ##fix the duration in the grid fitting
            parinfo2[6]['fixed'] = True
            bestpars             = pstart*0.0
            current_chi2         = np.inf ##not going to store anything except the current best solution on the grid
            for l in range(len(lgrid)):
                pstart[6] = lgrid[l]
                #dfit      = mpfit(transit_window_slide, pstart, functkw=fa,quiet=False,parinfo=parinfo)             
                dfit2,extrares = mpyfit.fit(transit_window_slide_pyfit,pstart,args=(wdat.t,wdat.fraw,error,wdat.s,thistime),parinfo=parinfo2,maxiter=200) ##args = t,fl,sig_fl,s,ttime
                if extrares['bestnorm'] < current_chi2:
                    bestpars     = dfit2*1.0
                    current_chi2 = extrares['bestnorm']*1.0
            ##Now that best grid solution is found, unfix the transit duration paramater
            parinfo2[6]['fixed'] =True
        
            ##run the fit with no fixed parameters but from the best grid point result from above      
            pstart  = bestpars
            pars,extrares = mpyfit.fit(transit_window_slide_pyfit,pstart,args=(wdat.t,wdat.fraw,error,wdat.s,thistime),parinfo=parinfo2,maxiter=200)    
            themod,modpoly,modnotch,modresid = transit_window_slide_pyfit(pars,args=(wdat.t,wdat.fraw,error,wdat.s,thistime),model=True)

            ##remove high outliers (like flares) and run again, do this based on a statistical measure like rms
            ##loop a few times to converge on an outlier-less solution 
            ##run the outlier rejection 5 times, that should be enough....
            pos_outlier = np.zeros(0,int)
            cliperr     = error*1.0
            oldnum=0

       
            
        
            for lll in range(0,5):
                koff     = np.where(np.isinf(cliperr) == False)[0]
                rms      = np.sqrt(np.mean((wdat.fraw[koff]-themod[koff])**2))
                rmsoff   = (wdat.fraw - themod)/rms
                knownbad = np.where(np.isinf(cliperr))[0]
                if np.isnan(rms) | (rms ==0): break
                    
                                        
                ##identify outliers
                new_outl    = np.where((rmsoff > cliplim) | (rmsoff < -cliplim))[0]
                pos_outlier = np.unique(np.append(pos_outlier,new_outl))
                cliperr[:]  = rms*1.0
                cliperr[pos_outlier] = np.inf   ##set outlier point uncertainties to effective infinity
                cliperr[knownbad]    = np.inf   ##set outlier point uncertainties to effective infinity

                if (len(pos_outlier) > oldnum) | (lll==0): ##dont refit unless theres a new point flagged as an outlier.
                    fa      = {'fl':wdat.fraw, 'sig_fl':cliperr, 't':wdat.t,'s':wdat.s,'ttime':thistime}
                    args=(wdat.t,wdat.fraw,error,wdat.s,thistime)
                    pars,thisfit = mpyfit.fit(transit_window_slide_pyfit, pstart, args=(wdat.t,wdat.fraw,cliperr,wdat.s,thistime),parinfo=parinfo2) 
                    themod,modpoly,modnotch,modresid = transit_window_slide_pyfit(pars, args=(wdat.t,wdat.fraw,cliperr,wdat.s,thistime), model=True)

                oldnum = len(pos_outlier)
            ##calculate the final model chi2 here, need to recalc because the rms number might have changed after the last outlier rejection and model fit
            modchi2 = np.sum(((themod-wdat.fraw)/cliperr)**2)

            ##try model with no transit, i.e fix the depth to zero
            parinfo2[5]['fixed'] = True
            nullstart           = pars.copy()
            nullstart[5]        = 0.0
               
            nullfit,nullextra = mpyfit.fit(transit_window_slide_pyfit, nullstart,  args=(wdat.t,wdat.fraw,cliperr,wdat.s,thistime),parinfo=parinfo2) 
            nullmod,nullpoly,dummy,nullresid = transit_window_slide_pyfit(nullfit, args=(wdat.t,wdat.fraw,cliperr,wdat.s,thistime),model=True)
            nullchi2 = np.sum(nullresid**2)
        
            ##what are the two model bayesian information criteria?
            numpars = len(parinfo2) - np.sum([parinfo2[dude]['fixed'] for dude in range(len(parinfo2))])
            npoints  = len(np.where(cliperr < 0.99)[0])
            bicnull  = nullchi2 + (numpars-2)*np.log(npoints)
            bicfull  = modchi2  + numpars*np.log(npoints)
        
            ##make figures or check things?
            #if i >= 1e90: 
                ##plotting moviemaking stuff, usually not run.
                #import pdb
                #import matplotlib.pyplot as plt
                #pdb.set_trace()
                #plt.plot(wdat.t,wdat.fraw,'.')
                #plt.plot(wdat.t,themod,'r')
                #plt.plot(wdat.t,modpoly,'g')
                #plt.show()
                #import pdb
                #pdb.set_trace()
                # mmm = np.zeros(len(wdat.t),dtype=int)
    #             mmm[pos_outlier] = -1
    #             oks = np.where((mmm >= 0))[0]
    #             ax1 = plt.subplot(211)
    #             plt.plot(wdat.t[oks],wdat.fraw[oks],'ko',label='K2 Data')
    #             plt.plot(wdat.t[ttt],wdat.fraw[ttt],'m*',markersize=18)
    #             plt.plot(wdat.t,themod,'r',label = 'Poly + Notch')
    #             plt.plot(wdat.t,modpoly,'r-.')
    #             plt.plot(wdat.t,nullpoly,'--g',label='Poly')
    #             plt.plot(wdat.t[pos_outlier],wdat.fraw[pos_outlier],'rx',markeredgecolor='r',mew=2)
    #             if i <= 460:  plt.legend(loc='upper right')
    #             ax1.set_ylabel('Relative Flux')
    #             ax1.ticklabel_format(useOffset=False, style='plain')
    # 
    #             ax2 = plt.subplot(212)
    #             oks2 = np.where((fittimes < fittimes[i]))[0]
    #             plt.plot(fittimes[oks2]-fittimes[0],detrend[oks2],'k.')
    #             plt.plot(fittimes[wind[ttt]]-fittimes[0],wdat.fraw[ttt]/modpoly[ttt],'*m',markersize=18)
    #             beyond = np.where(fittimes > fittimes[i])[0]
    #             plt.plot(fittimes[beyond]-fittimes[0], data.fcor[beyond],'g.')
    #             ax2.set_ylabel('Relative Flux')
    #             ax2.set_xlabel('Time (days)')
    #             if i <= 460:
    #                 ax2.set_xlim([5,16])   
    #                 ax2.set_ylim([0.985,1.02])                          
    #             if i >460:
    #                 ax2.set_ylim([0.975,1.02])                         
    #                 ax2.set_xlim([0.00,fittimes[-1]-fittimes[0]])       
    #             ax2.ticklabel_format(useOffset=False, style='plain')
    #             if i <= 460:
    #                 for nnn in range(100): plt.savefig('moviemake/'+str(i)+'_'+str(nnn)+'_movmake.png')
    #             if i > 460:
    #                 plt.savefig('moviemake/'+str(i)+'_0'+'_movmake.png')
    #            #import pdb
    #            #pdb.set_trace()
    #            
    #            plt.clf()
            
        
            ##if the bayesian information criterion for the full and null models provide evidence against the transit notch, 
            ##just use the null model for the fit, the difference required to believe a transit notch is requires is by
            ##default 0, which is very inclusive, but can be tuned if you know what you're looking for. 
            ##deltaBIC > 0 means notch is favoured.
        
            if bicnull - bicfull < deltabic: 
                modpoly = nullmod ##if notch not required, use the null model
                pars[5] = 0.0     ##set to zero for later storage
            if  i > np.inf: ##for Aaron to look at whats getting notched, set i>-1 to see everything
                print fittimes[i]
                print bicnull-bicfull
                print pars[5]
                print np.min(cliperr)
                print rms
                import pdb
                import matplotlib.pyplot as plt
                plt.plot(wdat.t,wdat.fraw,'.')
                plt.errorbar(wdat.t,wdat.fraw,yerr = cliplim*0.0+rms,fmt='b.')
                plt.plot(wdat.t,themod,'g')
                plt.plot(wdat.t,nullmod,'r')
                plt.plot(wdat.t[ttt],wdat.fraw[ttt],'m*',markersize=18)
                plt.plot(wdat.t[pos_outlier],wdat.fraw[pos_outlier],'rx',markeredgecolor='r',mew=2)
                plt.show()

            
        ##save things we care about, like a detrended curve and a transit depth fit for this point in time
        ttt           = np.where(wdat.t == thistime)[0]
                
        detrend[i]    = wdat.fraw[ttt]/modpoly[ttt]
        polyshape[i]  = modpoly[ttt]*1.0
        depthstore[i] = pars[5]*1.0

        ##make sure outliers get flagged appropriately
        badflag[wind[pos_outlier]] = 1
        badflag[wind[flare]] = 2
        if cleanmask[0] != -1:
            intrans = np.where(cleanmask == 99)[0]
            badflag[intrans] = 0
        
    ##spit output 
    return fittimes, depthstore, detrend, polyshape, badflag
  
##the LOCoR pipeline
##for now its called rcomb because I dont feel like changing all the calls.
##
def rcomb(data,prot,rmsclip=3.0,aliasnum=2.0,cleanmask=[-1,-1]):
    '''
    The Locally Optimized Combination of Rotations detrending method
    
    Inputs:
    (1) data: The data recarray that this code uses for passing data around
    (2) prot: The rotation period of this star measured by you elsewhere
    
    Optional Inputs:
    (1) rmsclip: Sigma-clipping level for outliers in the fit iterations default is 3
    (2) aliasnum: Period to alias the rotation period (prot) to. Needed when each period doesnt contain
    sufficient data for a good fit. Default is 2 days which works robustly for K2 long cadence data. Set to
    a number less than prot to not alias at all, e.g . set to -1.0.
    (3) cleanmask: binary mask to remove a set of points from the fitting. Great for masking over a transit signal that you 
    dont want influencing the fit. Default is [-1,-1] which turns it off 
        
    Outputs:
    (1) Times: The input time axis from data
    (2) Dummy: This is a dummy output of zeros to match the notch filter output format for compatibility
    (3) detrend: detrended lightcurve.f
    (4) finalmodel: the model used to detrend the input lightcurve
    (5) badflag: integer flags for each datapoint in data with 0=masked as outlier in iterations
        1=fine, 2=strong positive outlier or other suspect point.
    
    '''

    ##print 'aliasnum ' + str(aliasnum) ##testing line
    ##!!initial organizing
    ##calculate phases for this rotation period
    phase  = calc_phase(data.t,prot,data.t[0])
    ##locate each period, label it
    plabel = np.zeros(len(data.t),int) 
    point  = np.where(phase - np.roll(phase,1) < 0.0)[0]
    for i in range(len(point)-1): plabel[point[i]:point[i+1]] = i+1
    pers = np.unique(plabel)
    thelc = data.fcor*1.0
    ##offset each period to better line up with others, basically median divide each period
    for i in range(len(pers)):
        bnd = np.where(plabel == pers[i])[0]
        thelc[bnd]  = thelc[bnd]/np.nanmedian(thelc[bnd])



    ##!!Outlier Rejection
    ##now remove any outliers using lcbin, pretty simple (just marking them really)
    lbin,binsizerm,siglbin,goodind,phasebin = k2sff.lcbin(phase,thelc, 20,userobustmean=True)
    badflag = np.zeros(len(data.t),int)+0
    badflag[goodind] = 1
    ##any points less than 0, flag them as bad now (failure mode output of K2sff)
    qwe = np.where(data.fcor <= 0.0)[0]
    badflag[qwe] = 2
    
    ##for the case of masking a known transit out of the fit::
    if cleanmask[0] != -1:
        maskthese = np.where(cleanmask == 99)[0]
        badflag[maskthese] = 0
    
    
    finalmodel = thelc*0.0
    detrend    = thelc*0.0
   
    ##should we alias up the rotation period?
    ##If Prot < 2 (default number) alias it up to at least 2, also correct the phases to match new period
    newprot = prot*1.0
    if prot < aliasnum:
        pfactor    = int(np.ceil(aliasnum/prot))
        newprot    = prot*pfactor
    ##!Reorganize to new rotation period
    ##recalc phases and period labelling to newprot
    phase = calc_phase(data.t,newprot,data.t[0])
    plabel = np.zeros(len(data.t),int) 
    point  = np.where(phase - np.roll(phase,1) < 0.0)[0]
    for i in range(len(point)-1): plabel[point[i]:point[i+1]] = i+1
    
    ##stack each period of data and correct any bulk offsets again, 
    ##usually a very minor correction
    liblc = ()
    libph = ()
    libflag = ()
    keepindex = ()
    pers = np.unique(plabel)
    for i in range(len(pers)):
        bnd = np.where(plabel == pers[i])[0]
        keepindex += (bnd*1,)
        thisperiod = thelc[bnd]/np.nanmedian(thelc[bnd])
        liblc  += (thisperiod*1.0,)
        libph  += (phase[bnd]*1.0,)
        libflag +=(badflag[bnd]*1,)
    liblc     = np.array(liblc)
    libph     = np.array(libph)
    libflag   = np.array(libflag)
    keepindex = np.array(keepindex)
    stackmod  = liblc*0.0

    ##we now have a clean library of rotation periods with outliers flagged up. 
    
    ##now do the actual fit, go period by period
    for i in range(len(liblc)):
        ##extract the period of interest, along with outlier flags
        thisphase,thislc,thisflag = libph[i]*1.0,liblc[i]*1.0,libflag[i]*1
        
        ##if lots of points are somehow called outliers (some failure mode of robust mean)
        ##just use them all. This happens rarely.
        ##Also note this never changes badflg=2 points, they stay bad forever
        pointsin = np.where(thisflag == 0)[0] ##number of bad points
        ##if more than half bad, call them all good
        if len(pointsin) > len(thisflag)/2: thisflag[pointsin] =1  
        pointsin  = np.where(thisflag == 1)[0] ##how many we have now?
        reallybad = np.where(thisflag == 2)[0]
        thisflag[reallybad] = 0 ##reset all the failpoints to 0 flags
        ##ID the non-fitting periods
        oth = np.where(np.arange(len(liblc)) != i)[0]
        olc = ()
        
        
       
            
        ##Limit the number of periods to use so fit is not over specified.
        ##Ensures A matrix is non-sigular and invertible. This only matters for 
        ##a partial period e.g. at the end of the dataset.
        ##Currently taking the nearest rotation periods within this limit. 
        sizelim = len(pointsin)-3
        nearest = np.argsort(np.absolute(oth-i))
        oth = oth[nearest[0:sizelim]]
        
        ##now be careful, if the expected number of points is too small 
        ##just return the input values, also set badflag to 2 so they are removed from 
        ##BLS search later.
        ##this happens when the partial period at end of dataset is only a couple of points
        
        if (sizelim >= 1) & (-np.nanmin(thislc) + np.nanmax(thislc) > 0.00000000000001): 
        
        
            
            
            ##now interpolate all the reference periods onto the phase-scale of the 
            ##period we are fitting, being careful to not use the outliers.
            for j in range(len(oth)):  
                keep = np.where(libflag[oth[j]] == 1)[0]
                if (len(keep) > 5) & (len(libflag[oth[j]]) > len(thisflag)*2./3.): ##only keep a period if it has more than half its points as fine
                    if (-np.nanmin(liblc[oth[j]][keep]) + np.nanmax(liblc[oth[j]][keep]) > 0.00000000000001): olc  += (np.interp(thisphase,libph[oth[j]][keep],liblc[oth[j]][keep]),)    
            olc = np.array(olc)

            ##now check that the reference library actually has something in it.
            if len(olc) > 0:
                rrr=0
                while rrr < 3: ##do this at most three times, removing outliers each time.
                    ##reference library is ready to use now.
                    ##populate the A matrix, masking over outliers in sum with flag array
                    Amat = np.dot(olc*thisflag,olc.T)
                    ##populate b vector, again masking over outliers in the sum
                    bvect = np.dot(olc*thisflag,thislc)
                    ##do the inverse multiplication
                    coeffs = np.dot(bvect,np.linalg.inv(Amat))
                    themodel = 0.0
                    for j in range(len(olc)): themodel += olc[j]*coeffs[j] ##Sum up the model
            
                    
            
            
                    #now find the rmsoffset
                    totest = np.where(thisflag==1)[0]
                    mad = np.median(np.absolute((thislc[totest]/themodel[totest]-1)))/0.67
                    #rms = np.sqrt(np.mean((thislc/themodel*thisflag - thisflag)**2))
                    offset = (thislc*thisflag/themodel-1)/mad*thisflag
                    outl = np.where(np.absolute(offset) > rmsclip)[0]
                    
                    
                    
                    #print mad, rms
                    ##set the flag on this outlier to 0
                    if len(outl) > 0:
                        thisflag[outl] = 0 ##now repeat the process
                        badflag[keepindex[i][outl]] = 0 ##also store for long-term output
                    if len(outl) == 0 : rrr = 5
                    ##now check that we have enough points to actually do another fit
                    ## do this by truncating the library period array 
                    numberin = len(np.where(thisflag == 1)[0])
                    if numberin <= len(olc): 
                        rrr = 5
                        print 'limit hit for outliers'
                    rrr +=1
                    
                    
                    
                    
                    
                    #print 'removing ' + str(len(outl)) + ' outliers '  + str(rrr) 
                    
#                     import pdb
#                     import matplotlib.pyplot as plt
#                     import matplotlib as mpl
#                     for iii,ooo in enumerate(olc): plt.plot(thisphase,ooo,'.',markersize=5,color=mpl.cm.gray(iii*10))
#                     plt.plot(thisphase,thislc,'r.')
#                     plt.plot(thisphase,themodel,'sb')
#                     bbbb = np.where(thisflag != 1)[0]
#                     plt.plot(thisphase[bbbb],thislc[bbbb],'m.')
#                     plt.show()
#                     pdb.set_trace()
                    
                    
                    
#                     if rrr >=3:
#                         import pdb
#                         import matplotlib.pyplot as plt
#                         import matplotlib as mpl
#                         from matplotlib import gridspec
# 
#                         mpl.rcParams['lines.linewidth']   = 1.5
#                         mpl.rcParams['axes.linewidth']    = 2
#                         mpl.rcParams['xtick.major.width'] =2
#                         mpl.rcParams['ytick.major.width'] =2
#                         mpl.rcParams['ytick.labelsize'] = 15
#                         mpl.rcParams['xtick.labelsize'] = 15
#                         mpl.rcParams['axes.labelsize'] = 18
#                         mpl.rcParams['legend.numpoints'] = 1
#                         mpl.rcParams['axes.labelweight']='semibold'
#                         mpl.rcParams['font.weight']='semibold'
#                         qwe = np.where(thisflag == 0)[0]
#                     
#                         fig = plt.figure()
#                         gs = gridspec.GridSpec(2,1,height_ratios=[3,1])
#                         ax0 = plt.subplot(gs[0])
#                         ax1 = plt.subplot(gs[1])
#                         for iii,ooo in enumerate(olc): ax0.plot(thisphase,ooo,'.',markersize=5,color=mpl.cm.gray(iii*10))
#                         ax0.plot(thisphase,themodel,'sb',markersize=7)
#                         ax0.plot(thisphase,thislc,'r.',markersize=8)
#                         ax0.plot(thisphase[qwe],thislc[qwe],'m.',markersize=8)
#                         
#                         ax0.set_ylim([0.985,1.015])
#                         ax0.set_ylabel('Relative Brightness')
#                         #plt.tight_layout()                        
#                         #
#                         #plt.clf()                        
#                         
#                         okok = np.where(thisflag==1)[0]
#                         resid = thislc - themodel
#                         ax1.plot(thisphase[okok],resid[okok]*1000.0,'ok',markersize=6)
#                         ax1.plot([0.0,1.0],[0.0,0.0],'k--')
#                         ax1.set_ylabel('O-C (mmag)')
#                         ax1.set_xlabel('Rotational Phase')
#                         ax1.set_ylim([-5,5])
#                         plt.savefig('Rotational_fit_example_hyades_planet.pdf')
#                         plt.show()
#                         pdb.set_trace()

              
                ##what happens to the model at point where thisflag==0? should really remove the model
                ##and replace with interpolation from of actually fit points
                nofit = np.where(thisflag == 0)[0]
                if len(nofit) > 0 :
                    donefit = np.where(thisflag == 1)[0]
                    #pdb.set_trace()
                    themodel[nofit] = np.interp(thisphase[nofit],thisphase[donefit],themodel[donefit])
                #if i > 0:
                        #import pdb
                        #import matplotlib.pyplot as plt
                       # plt.plot(thisphase,thislc,'.')
                        #zxc = np.where(thisflag==0)[0]
                       # plt.plot(thisphase[zxc],thislc[zxc],'.m')
                       # plt.plot(thisphase,themodel,'.r')
                        #plt.show()
                                              
                        
                
            else: ##if no points, (fail case for k2sff) flag everything as bad, model is null
                themodel  = thislc*0.0+1.0 ##don't alter the points because fit failed
                badflag[keepindex[i]] = 2  ##flag as bad points
            
        else: ##case where there's not enough points in this phaseing, just call these points bad going foward
            themodel  = thislc*0.0+1.0 ##don't alter the points because fit failed
            badflag[keepindex[i]] = 2  ##flag as bad points
            
        ##put this period's fit into the appropriate spot in full array
        stackmod[i] = themodel*1.0
        finalmodel[keepindex[i]] = themodel*1.0
        detrend[keepindex[i]] = thislc/themodel


    ##now find all outliers that are positive and flag them as bad
    badflag[np.where((badflag ==0) & (detrend > 1.0))[0]] = 2
    return data.t, data.t*0.0, detrend, finalmodel, badflag
    
    
##experimental, do use unless you want to waste time, or you are aaron
def locor2(data,prot,rmsclip=3.0,aliasnum=2.0,cleanmask=[-1,-1],deltabic=0.0):  

    ##!!initial organizing
    ##calculate phases for this rotation period
    phase  = calc_phase(data.t,prot,data.t[0])
    ##locate each period, label it
    plabel = np.zeros(len(data.t),int) 
    point  = np.where(phase - np.roll(phase,1) < 0.0)[0]
    for i in range(len(point)-1): plabel[point[i]:point[i+1]] = i+1
    pers = np.unique(plabel)
    thelc = data.fcor*1.0
    ##offset each period to better line up with others, basically median divide each period
    for i in range(len(pers)):
        bnd = np.where(plabel == pers[i])[0]
        thelc[bnd]  = thelc[bnd]/np.nanmedian(thelc[bnd])



    ##!!Outlier Rejection
    ##now remove any outliers using lcbin, pretty simple (just marking them really)
    lbin,binsizerm,siglbin,goodind,phasebin = k2sff.lcbin(phase,thelc, 20,userobustmean=True)
    badflag = np.zeros(len(data.t),int)+0
    badflag[goodind] = 1
    lbininterp = np.interp(phase,phasebin,lbin)
    tokeep = np.where((badflag == 0) & (lbininterp-thelc > 0.0))[0]
    badflag[tokeep] = 1
    #import pdb
    #import matplotlib.pyplot as plt
    #pdb.set_trace()
    ##any points less than 0, flag them as bad now (failure mode output of K2sff)
    qwe = np.where(data.fcor <= 0.0)[0]
    badflag[qwe] = 2
    
    ##for the case of masking a known transit out of the fit::
    if cleanmask[0] != -1:
        maskthese = np.where(cleanmask == 99)[0]
        badflag[maskthese] = 0
    
    
    finalmodel = thelc*0.0
    detrend    = thelc*0.0
    depthstore = thelc*0.0
    ##should we alias up the rotation period?
    ##If Prot < 2 alias it up to at least 2, also correct the phases to match new period
    newprot = prot*1.0
    if prot < aliasnum:
        pfactor    = int(np.ceil(aliasnum/prot))
        newprot    = prot*pfactor
    
    ##!Reorganize to new rotation period
    ##recalc phases and period labelling to newprot
    phase = calc_phase(data.t,newprot,data.t[0])
    plabel = np.zeros(len(data.t),int) 
    point  = np.where(phase - np.roll(phase,1) < 0.0)[0]
    for i in range(len(point)-1): plabel[point[i]:point[i+1]] = i+1
    
    ##stack each period of data and correct any bulk offsets again, 
    ##usually a very minor correction
    liblc = ()
    libph = ()
    libflag = ()
    keepindex = ()
    pers = np.unique(plabel)
    for i in range(len(pers)):
        bnd = np.where(plabel == pers[i])[0]
        keepindex += (bnd*1,)
        thisperiod = thelc[bnd]/np.nanmedian(thelc[bnd])
        liblc  += (thisperiod*1.0,)
        libph  += (phase[bnd]*1.0,)
        libflag +=(badflag[bnd]*1,)
    liblc     = np.array(liblc)
    libph     = np.array(libph)
    libflag   = np.array(libflag)
    keepindex = np.array(keepindex)
    stackmod  = liblc*0.0

    ##we now have a clean library of rotation periods with outliers flagged up. 
    
    ##now do the actual fit, go period by period
    for i in range(len(liblc)):
        print i
        ##extract the period of interest, along with outlier flags
        thisphase,thislc,thisflag = libph[i]*1.0,liblc[i]*1.0,libflag[i]*1
        
        ##if lots of points are somehow called outliers (some failure mode of robust mean)
        ##just use them all. This happens rarely.
        ##Also note this never changes badflg=2 points, they stay bad forever
        pointsin = np.where(thisflag == 0)[0] ##number of bad points
        ##if more than half bad, call them all good
        if len(pointsin) > len(thisflag)/2: thisflag[pointsin] =1  
        pointsin  = np.where(thisflag == 1)[0] ##how many we have now?
        reallybad = np.where(thisflag == 2)[0]
        thisflag[reallybad] = 0 ##reset all the failpoints to 0 flags
        ##ID the non-fitting periods
        oth = np.where(np.arange(len(liblc)) != i)[0]
        olc = ()
        
        
       
            
        ##Limit the number of periods to use so fit is not over specified.
        ##Ensures A matrix is non-sigular and invertible. This only matters for 
        ##a partial period e.g. at the end of the dataset.
        ##Currently taking the nearest rotation periods within this limit. 
        sizelim = len(pointsin)-3
        nearest = np.argsort(np.absolute(oth-i))
        oth = oth[nearest[0:sizelim]]
        
        ##now be careful, if the expected number of points is too small 
        ##just return the input values, also set badflag to 2 so they are removed from 
        ##BLS search later.
        ##this happens when the partial period at end of dataset is only a couple of points
        periodmodel = thislc*0.0+1.0
        perioddepth = thislc*0.0
        if (sizelim >= 1) & (-np.nanmin(thislc) + np.nanmax(thislc) > 0.00000000000001): 
        
        
            
            
            ##now interpolate all the reference periods onto the phase-scale of the 
            ##period we are fitting, being careful to not use the outliers.
            for j in range(len(oth)):  
                keep = np.where(libflag[oth[j]] == 1)[0]
                if (len(keep) > 5) & (len(libflag[oth[j]]) > len(thisflag)*2./3.): ##only keep a period if it has more than half its points as fine
                    if (-np.nanmin(liblc[oth[j]][keep]) + np.nanmax(liblc[oth[j]][keep]) > 0.00000000000001): olc  += (np.interp(thisphase,libph[oth[j]][keep],liblc[oth[j]][keep]),)    


            olc += (olc[0]*0.0,) ##add the final part for the hat function to be plonked into
            origolc = np.array(olc)  ##make it an easier to access numpy array
            ##now check that the reference library actually has something in it.
            if len(olc) > 1:
                phasewidths = np.array([0.75,1.0,2.0,4.0,0.0])/24.0/newprot
                
                for cad in range(len(thisphase)):##go point by point in this rotation pseudo-period
                    #print cad
                    pointphase = thisphase[cad]
                      

                    ##first try fitting with the different window sizes
                    buseflag = thisflag*0 -1
                    thischi2 = np.inf
                    bestcoeffs = 0.0*len(origolc) - 1
                    bestwidth  = -100.0
                    for wwind, www in enumerate(phasewidths):
                        ##add hat shape transit function to OLC
                        ##make sure everythings modded right to capture full transit
                        useflag = thisflag*1
                        olc = origolc*1.0
                        olc[-1,:] = 0.0
                        if wwind < len(phasewidths)-1: 
                            shifter = 0.5-pointphase
                            wpoints = np.where((np.mod(thisphase+shifter,1.0) < 0.5+www/2.0) & (np.mod(thisphase+shifter,1.0)> 0.5-www/2.0))[0]
                        else: wpoints = np.array([],dtype=int)
                        olc[-1,wpoints] = 1.0
                        if len(wpoints) == 0: olc = olc[:-1] ##if null model remove notch part of matrix olc
                        if np.sum(olc[-1]*useflag) == 0 : olc = olc[:-1] ##if all points in notch are called outliers, remove notch part of olc
                        #if cad == 9:
                        #    import pdb
                        #    pdb.set_trace()
                        ##make up new flag arrays
                        
                        
                    
                        rrr=0
                        while rrr < 3: ##do this at most three times, removing outliers each time.
                            ##reference library is ready to use now.
                            ##populate the A matrix, masking over outliers in sum with flag array
                            Amat = np.dot(olc*useflag,olc.T)
                            ##populate b vector, again masking over outliers in the sum
                            bvect = np.dot(olc*useflag,thislc)
                            ##do the inverse multiplication
                            coeffs = np.dot(bvect,np.linalg.inv(Amat))
                            themodel = 0.0
                            for j in range(len(olc))  :  themodel += olc[j]*coeffs[j] ##Sum up the model
                            polymod = 0.0
                            for j in range(len(olc)-1)  : polymod += olc[j]*coeffs[j] ##Sum up the model
                                        
                            #now find the rmsoffset
                            totest  = np.where(useflag==1)[0]
                            mad     = np.median(np.absolute((thislc[totest]/themodel[totest]-1)))/0.67
                            offset  = (thislc*thisflag/themodel-1)/mad*thisflag
                            outl    = np.where(np.absolute(offset) > rmsclip)[0]
                            modchi2 = np.sum((themodel[totest]-thislc[totest])**2/mad**2)
                    
                    
                            #print mad, rms
                            ##set the flag on this outlier to 0
                            if len(outl) > 0:
                                useflag[outl] = 0 ##now repeat the process
                                if np.sum(olc[-1]*useflag) == 0 : olc = olc[:-1] ##if all points in notch are called outliers, remove notch part of olc
                                 ##also store for long-term output
                            if len(outl) == 0 : rrr = 5
                            ##now check that we have enough points to actually do another fit
                            ## do this by truncating the library period array 
                            numberin = len(np.where(useflag == 1)[0])
                            if numberin <= len(olc): 
                                rrr = 5
                                print 'limit hit for outliers'
                            rrr +=1
                        #print modchi2
                        if (modchi2 < thischi2) & (wwind < len(phasewidths)-1): ##if this model is better than previous models, save it's information
                            buseflag   = useflag*1
                            thischi2   = modchi2*1.0
                            bestcoeffs = coeffs*1.0
                            bestwidth  = www*1.0  
                            bestmodel = themodel*1.0
                            bestpoly  = polymod*1.0
                        if wwind == len(phasewidths)-1:
                            nullflag   = useflag*1
                            nullchi2   = modchi2*1.0
                            nullcoeffs = coeffs*1.0
                            nullwidth  = 0.0
                            nullmodel  = themodel*1.0
                    
                    bicnull = nullchi2 + (len(origolc)-1)*np.log(np.sum(nullflag))
                    bicfull = thischi2 + len(origolc)*np.log(np.sum(buseflag))
                    if (bicnull-bicfull > deltabic) & (bestcoeffs[-1] < 0.0):
                        usepoly = bestpoly*1.0
                        usedepth = -1.0*bestcoeffs[-1]
                        useflag = buseflag*1
                    else:
                        usepoly = nullmodel*1.0
                        usedepth = 0.0
                        useflag = nullflag*1
                    periodmodel[cad] = usepoly[cad]*1.0
                    perioddepth[cad] = usedepth*1.0
                    #import pdb
                    #import matplotlib.pyplot as plt
                    #pdb.set_trace()

                    import pdb
                    import matplotlib.pyplot as plt
                    qwe = np.where(perioddepth > 0.0)[0]
                    plt.plot(thisphase,thislc,'b.')
                    plt.plot(thisphase,bestpoly,'g.')
                    plt.plot(thisphase,bestmodel,'r.')
                    qwe = np.where(buseflag == 0)[0]
                    plt.plot(thisphase[qwe],thislc[qwe],'xm')
                    
                    plt.plot(thisphase,nullmodel,'m.')
                    plt.plot(thisphase[cad],thislc[cad],'ob')
                    plt.show()
                    pdb.set_trace()
                    
            else: ##if no points, (fail case for k2sff) flag everything as bad, model is null
                periodmodel  = thislc*0.0+1.0 ##don't alter the points because fit failed
                badflag[keepindex[i]] = 2  ##flag as bad points
            
        else: ##case where there's not enough points in this phaseing, just call these points bad going foward
            periodmodel  = thislc*0.0+1.0 ##don't alter the points because fit failed
            badflag[keepindex[i]] = 2  ##flag as bad points
            
        ##put this period's fit into the appropriate spot in full array
        stackmod[i] = periodmodel*1.0
        finalmodel[keepindex[i]] = periodmodel*1.0
        detrend[keepindex[i]] = thislc/periodmodel
        depthstore[keepindex[i]] = perioddepth*1.0
        import pdb
        import matplotlib.pyplot as plt
        qwe = np.where(perioddepth > 0.0)[0]
        plt.plot(thisphase,thislc,'b.')
        plt.plot(thisphase[qwe],thislc[qwe],'g.')
        plt.plot(thisphase,periodmodel,'r.')
        plt.show()
        
        
        #pdb.set_trace()

    ##now find all outliers that are positive and flag them as bad
    badflag[np.where((badflag ==0) & (detrend > 1.0))[0]] = 2
    return data.t, depthstore, detrend, finalmodel, badflag    
  
       
     
##SNR calculation for BLS power spectra that uses a median-absolut-deviation estimator for the 
##half normal distribution stdev, works fairly well for most periods. There could be issues if you search a huge span of periods where red noise effects statistics.
def bls_power_analysis_snr(pgrid,power):
    powbin,binsizerm,pebinm,goodind,perbin = k2sff.lcbin(np.log(pgrid), power, 15,userobustmean=True)
    trendpow = np.interp(np.log(pgrid),perbin,powbin)
    flatpow  = power - trendpow
    snr      = flatpow/(np.median(np.absolute(np.median(flatpow) - flatpow))/0.6745)
    bestspot = np.argmax(snr)
    bestp    = pgrid[bestspot]
    bestsnr  = snr[bestspot]
    return bestp,bestsnr

      
  
##run to search for transit like signals in a detrended lightcurve, basically a stand-alone copy of what detrend_k2 does
##data has to look like the recarrays used everywhere else in this code
def bls_transit_search(data,detrend,badflag,rmsclip=3.0,snrcut=7.0,cliplow = 30.0,binn=300,period_matching=[-1,-1,-1],searchmax=30.0,searchmin=1.0000001,mindcyc=0.005,maxdcyc=0.3):
##assess the output flags from the detrending
    mmm = np.ones(len(data.t),dtype=int)
    bad = np.where(((data.s<0.0) | (data.s>8)) & (detrend<0.99) | (detrend <=0.0))[0]
    mmm[bad] = 0
    
    ##remove other high points, they can hide transits from BLS    
    extracut1 = np.percentile(detrend,99.9)##don't plot the highest 0.1% of points, they are either garbage or not important for us now
    extracut2 = np.percentile(detrend,0.1)##don't plot the highest 0.1% of points, they are either garbage or not important for us now
    qwe = np.where((detrend < extracut1) & (detrend > extracut2))[0]
    lcrms = np.sqrt(np.nanmean((1.0-detrend[qwe])**2))
    good = np.where((badflag < 2)  & (mmm == 1) & (detrend < rmsclip*lcrms+1.0))[0] ##(detrend > 1.0 - cliplow*lcrms)

    flar = np.where(badflag == 2)[0]
    pout = np.where(badflag == 1)[0]
    bad  = np.where(badflag == 3)[0]


    ##The transit search on the detrended lightcurve, using BLS.

    ##setup the bls stuff
    uvect = data.t.copy()*0.0
    vvect = data.t.copy()*0.0
    fstep = 0.00002
    #if searchmax <= 1.0: fstep = 0.01 ##for USP mode, try not to over period, not sure this is needed anymore
    freq  = np.arange(1./searchmax,1./searchmin,fstep)
    pgrid = 1.0/freq
    
    firstpower = uvect*1.0*0.0 ##set a firstpower for output even when the torun cut fails
    firstphase = uvect*1.0*0.0 ##set a firstpower for output even when the torun cut fails
    ##run the transit search in a loop, masking each detected signal
    ##10 should be more than enough
    mask   = np.ones(len(good),dtype=int)
    dp     = np.zeros(10)
    best_p = np.zeros(10)
    detsig = np.zeros(10)
    t0     = np.zeros(10)
    dcyc   = np.zeros(10)


    for cnt in range(10):
        torun = np.where(mask == 1)[0] ##region not masked out

        if (len(torun) <= binn*2): ##check there's enough points to do a transit search
            dp     = dp[0:cnt+1]
            best_p = best_p[0:cnt+1]
            t0     = t0[0:cnt+1]
            detsig = detsig[0:cnt+1]
            dcyc = dcyc[0:cnt+1]
            break ## if we've mask so many points there's not enough for a binning, end.
            
        if (np.nanmax(data.t[good[torun]]) - np.nanmin(data.t[good[torun]]) < np.max(pgrid)+1.0): #check theres a decent span of time, separate to above due to error conditions
            dp     = dp[0:cnt+1]
            best_p = best_p[0:cnt+1]
            t0     = t0[0:cnt+1]
            detsig = detsig[0:cnt+1]
            dcyc = dcyc[0:cnt+1]
            break ## if lots of bad points, kill
        
        #THIS is the BLS search now        
        power, thisbest_p,best_pow,thisdp,qnum,in1,in2 = bls.eebls(data.t[good[torun]],detrend[good[torun]],uvect[good[torun]],vvect[good[torun]],len(pgrid),freq[0],fstep,binn,mindcyc,maxdcyc)
                                                        
        if cnt == 0: firstpower = power*1.0 ##save the most raw power spectrum for outputting
        
        ##Determine the highest SNR peak by flattening first
        bbp, snr  = bls_power_analysis_snr(pgrid,power) 
        best_p[cnt] = bbp*1.0
        detsig[cnt] = snr*1.0
        
        ##center the transit at phase 0.5 for ease of use later        
        ##need to rerun the BLS on the best point from the good analysis to do this
        dpower, dthisbest_p,dbest_pow,dthisdp,dqnum,din1,din2 = bls.eebls(data.t[good[torun]],detrend[good[torun]],uvect[good[torun]],vvect[good[torun]],1,1.0/best_p[cnt],fstep,binn,mindcyc,maxdcyc)        
        #import pdb
        #pdb.set_trace()
        dcyc[cnt] = dqnum*1.0
        dp[cnt]   = dthisdp*1.0 ##store the depth
        if din1 > din2: din1 -= binn
             
        rezero    = ((din1/2.0+din2/2.0)/binn-0.5)*best_p[cnt]
        t0[cnt]   = data.t[0]+rezero
        phase     = calc_phase(data.t,best_p[cnt],t0[cnt])
        if cnt == 0 : firstphase = phase*1.0 #if this is the first detection, store the phase for output for ease of use in other places

        ##exit the search loop if the current planet is below the SNR limit 
        if ((snr <= snrcut) & (cnt >-1)) | (best_p[cnt] < np.min(pgrid)): 
            dp     = dp[0:cnt+1]
            best_p = best_p[0:cnt+1]
            t0 = t0[0:cnt+1]
            detsig = detsig[0:cnt+1]
            dcyc = dcyc[0:cnt+1]
            break
        else: ##a detection, start with period matching for injection recovery if asked to, then mask the current signal and move on
            if period_matching[0] > 0.0:
                pdet = False
                ddet = False
                tdet = False
                ##Check the period is within 1% of input
                if (best_p[cnt]/period_matching[0] < 1.01) & (best_p[cnt]/period_matching[0] >0.99): pdet = True ##period match condition
                ##Check the depth is loosely ok (and not positive)
                if (dp[cnt]/period_matching[1] < 1.5)  & (dp[cnt]/period_matching[1] > 0.2): ddet = True ##depth match condition 
                ##now check t0 is within 1% of the period, requires some moduloing:
                if np.mod(np.absolute(t0[cnt] + 0.5*best_p[cnt]-period_matching[2]), period_matching[0])/period_matching[0] <  0.01: tdet = True
                
                if (pdet == True) & (ddet == True) & (tdet == True): return 1,best_p[cnt],t0[cnt]+0.5*best_p[cnt],dp[cnt] ##we found the injected planet, no need to continue

            ##if not period matching, mask this detection and research
            ##in transit points are based on BLS qnum, with 50% added to ingress/egress to match binning issues
            trp = np.where((phase[good] < 0.5+dqnum*2.0) & (phase[good] > 0.5-dqnum*2.0))[0]
            # import pdb
#             import matplotlib.pyplot as plt
#             plt.plot(phase[good],detrend[good],'b.')
#             plt.plot(phase[good[trp]],detrend[good[trp]],'r.')
#             plt.show()
#             pdb.set_trace()

            mask[trp] = 0
            
            
    ##the case where we've hit the sensitivity limit and the injected planet has not been found yet.
    if period_matching[0] > 0.0: return 0, -1, -1, -1
    
    ##otherwise return everything else:
    
    return best_p,dp,t0,detsig,firstpower,pgrid,firstphase,dcyc
  
    

##wrapper function that runs the detrending in a controlled way, including reading/writing data.
def do_detrend(cnum,epic,arclength=False, raw=False, wsize=1.0,totalfilename='', data_dir='/Volumes/UT2/UTRAID/K2raw/',outdir='',saveoutput=True,resolvabletrans=False,k2sff=False,indata=np.array([False,False]),idstring='',known_period=[-1,-1,-1],known_period2 = [-1,-1,-1],deltabic=-1.0,cleanup=False,period_matching=[-1,-1],snrcut=7.0,demode=1,max_period=30.0,min_period=1.00001,alias_num=2.0):
    '''
    Wrapper for running the notch filter and LOCoR detrending and BLS searches 
    
    Currently the inputs are not ideal, theres some decluttering to do...
    
    Inputs:Coming soon
  
    
    Optional Inputs:Coming soon, lots
   
        
    Outputs: Coming soon, lots of options
  
    '''
 
    detection = 0
    ##figure out the filename for reading in 
    cdir      = 'c'+str(cnum) + '/'
    if (type(cnum) == type(0)) & (cnum < 10): 
        cstrng = str(cnum)
        cstrng = '0'+cstrng
    else: cstrng = str(cnum)
    #i#mport pdb
    #pdb.set_trace()
    filename  = data_dir+cdir+'hlsp_k2sff_k2_lightcurve_' + str(epic) + '-c'+cstrng + '_kepler_v1_llc.fits'
    ##if complete path to the datafile is given use that only
    if totalfilename != '': filename = totalfilename
    
    
    
    ##define the output directory for results:
    if outdir == '': outdir = data_dir+cdir+'results/' 
    ##output directory for figures
    figdir = outdir + 'figures/'

    ##read the data file 
    if (k2sff == False) & (type(indata[0]) == type(np.array([False])[0])): data = read_datafile_vanderburg(filename,clean=True)
    if (k2sff == True) & (type(indata[0]) == type(np.array([False])[0])): 
        filename = data_dir + 'ep'+str(epic)+'.idl'
        if idstring != '': filename = data_dir + 'ep'+str(epic)+'_'+idstring+'.idl'
        data,dummy1,dummy2 = read_datafile_avextract(filename,clean=True)
    if type(indata[0]) != type(np.array([False])[0]): 
        data = indata.copy() ## the case where we passed the data directly
        #print 'case for input data'
    
    
    ##if we known where the planet is and want to aggressively detrend with a transit mask
    ##make a mask array for use in sliding window
    transmask=[-1,-1]
    if (known_period[0] != -1) & (cleanup == True):
        kphase = calc_phase(data.t,known_period[0],known_period[1])
        keep   = np.where((kphase < known_period[2]) | (kphase > known_period[3]))[0]
        transmask = np.zeros(len(kphase),dtype=int)+99
        transmask[keep] = 0
    
        
        
        
    ##run the detrending sliding notch fitter, LOCoR, or Something Else???!?
    if demode == 1: fittimes, depth, detrend,polyshape,badflag = sliding_window(data,windowsize=wsize,use_arclength=arclength,use_raw=raw,deltabic=deltabic,resolvable_trans=resolvabletrans,cleanmask=transmask) ##Notch Filter
    if demode == 2: fittimes,depth,detrend,polyshape,badflag = rcomb(data,wsize,cleanmask=transmask,aliasnum=alias_num) ##LOCoR
    if demode == 3: fittimes,depth,detrend,polyshape,badflag = locor2(data,wsize,cleanmask=transmask) ##not finished, experimental
        
    ##for the cleanup case, don't bother with trying to find transits:
    if cleanup == True: return fittimes, depth, detrend,polyshape,badflag 
        
    ##run the bls search, rmsclip should really be =1
    if period_matching[0] < 0: 
        best_p,dp,t0,detsig,firstpower,pgrid,firstphase,dcyc =  bls_transit_search(data,detrend,badflag,rmsclip=1.5,snrcut=snrcut,period_matching=period_matching,searchmax=max_period,searchmin=min_period)
        ##now clean out high-arclength points in each division, then retry

        divs = np.unique(data.divisions)
        runmask = np.zeros(len(data.t),int) 
        for dd in range(len(divs)):
            thisdiv = np.where(data.divisions == divs[dd])[0]
            ##kill 5 % on each side 
            keep = np.where((data.s[thisdiv] < np.percentile(data.s[thisdiv],96)) & (data.s[thisdiv] > np.percentile(data.s[thisdiv],4)))[0]
            runmask[thisdiv[keep]] = 1
        
        gomask = np.where(runmask == 1)[0]

        if len(divs) == 1:
            print 'skipping gomask search'
        ##now rerun BLS with these good points only
        if len(divs) > 1:
            best_p2,dp2,t02,detsig2,firstpower,pgrid2,firstphase,dcyc2 =  bls_transit_search(data[gomask],detrend[gomask],badflag[gomask],rmsclip=1.5,snrcut=snrcut,period_matching=period_matching,searchmax=max_period,searchmin=min_period)
        
            best_p = np.concatenate((best_p2,best_p))
            dp = np.concatenate((dp2,dp))
            t0 = np.concatenate((t02,t0))
            detsig = np.concatenate((detsig2,detsig))
            dcyc = np.concatenate((dcyc2,dcyc))
        
        
        ##for the demode=2 case, run again with all detrend outliers clipped
        if (demode == 2) | (demode == 3):
            gomask = np.where(badflag == 1)[0]
            best_p3,dp3,t03,detsig3,firstpower3,pgrid3,firstphase,dcyc3 =  bls_transit_search(data[gomask],detrend[gomask],badflag[gomask],rmsclip=1.5,snrcut=snrcut,period_matching=period_matching,searchmax=max_period,searchmin=min_period)
            best_p = np.concatenate((best_p,best_p3))
            dp = np.concatenate((dp,dp3))
            t0 = np.concatenate((t0,t03))
            detsig = np.concatenate((detsig,detsig3))
            dcyc = np.concatenate((dcyc,dcyc3))
        
        
        
    ##injection recovery input matching section here:
    if period_matching[0] > 0: 
        pmatch_result =  bls_transit_search(data,detrend,badflag,rmsclip=1.0,snrcut=snrcut,period_matching=period_matching,searchmax=max_period,searchmin=min_period)
        if pmatch_result[0] == 0: #now run with the divisional arclength cut 
         ##now clean out high-arclength points in each division, then retry
            divs = np.unique(data.divisions)
            runmask = np.zeros(len(data.t),int) 
            for dd in range(len(divs)):
                thisdiv = np.where(data.divisions == divs[dd])[0]
                ##kill 4% on each side 
                keep = np.where((data.s[thisdiv] < np.percentile(data.s[thisdiv],96)) & (data.s[thisdiv] > np.percentile(data.s[thisdiv],4)))[0]
                runmask[thisdiv[keep]] = 1
            ##now run with the new arclength mask for each division
       
            gomask = np.where(runmask == 1)[0]
            if len(divs) == 1: print 'skipping gomask search'
            if len(divs) > 1:
                pmatch_result =  bls_transit_search(data[gomask],detrend[gomask],badflag[gomask],rmsclip=1.0,snrcut=snrcut,period_matching=period_matching,searchmax=max_period,searchmin=min_period)
        
            ##for demode=2 case, run again with all detrend outliers clipped, if still not finding injected signal
            if (pmatch_result[0] == 0) & (demode == 2):
                gomask = np.where(badflag==1)[0]
                pmatch_result =  bls_transit_search(data[gomask],detrend[gomask],badflag[gomask],rmsclip=1.0,snrcut=snrcut,period_matching=period_matching,searchmax=max_period,searchmin=min_period)
        
        
        return pmatch_result
    
    ##if not saving the output, return useful variables
    if saveoutput == False: return data,fittimes,depth,detrend,polyshape,badflag,pgrid,firstpower,firstphase,detsig,best_p,dp,t0,dcyc
    
    ##start outputting here, if instructed to, this makes outputfiles and lots of plots
    if saveoutput == True:
        mmm = np.ones(len(data.t),dtype=int)
        bad = np.where(((data.s<0.0) | (data.s>8)) & (detrend<0.99) | (detrend <=0.0))[0]
        mmm[bad] = 0
    
        ##remove other high points, they can hid transits from BLS
        lcrms = np.sqrt(np.nanmean((1.0-detrend)**2))
        good = np.where((badflag < 2)  & (mmm == 1) & (detrend < 2**lcrms+1.0))[0]
        import matplotlib.pyplot as plt
        savefile = outdir+'detrend_EPIC'+str(epic)+'.pkl'
        pickle.dump((data,fittimes,depth,detrend,polyshape,badflag,pgrid,firstpower,firstphase,detsig,best_p,dp,t0,dcyc),open(savefile,'wb'))
        ##save the detrended LC regardless of the presence of a detection:
        #unphased plot
        plt.plot(data.t[good]-data.t[0],detrend[good],'.')
        ax = plt.subplot(111)
        ax.set_ylim([np.max([0.98,np.min(detrend[good])-0.001]),np.min([np.max(detrend[good])+0.001,1.01])])   
        ax.set_xlabel('Time (days)') 
        ax.set_ylabel('Relative Brightness')
        xlab = ax.xaxis.get_label()
        ylab = ax.yaxis.get_label()
        xlab.set_weight('bold')
        xlab.set_size(12)
        ylab.set_weight('bold')
        ylab.set_size(12)
        [i.set_linewidth(2) for i in ax.spines.itervalues()]
        ax.xaxis.set_tick_params(width=2,labelsize=10)
        ax.yaxis.set_tick_params(width=2,labelsize=10)
        plt.savefig(figdir + 'detrend_EPIC'+str(epic)+'.pdf')
        plt.clf()
        
        ##if a reasonable planet detection is found
        ##phased plot
        if (detsig[0] > snrcut):
            for cnt in range(len(detsig)):
                if detsig[cnt] >= 7.0:
                    detection = 1
                    thisphase = calc_phase(data.t,best_p[cnt],t0[cnt])
                    plt.plot(thisphase[good],detrend[good],'.')
                    ax = plt.subplot(111)
                    ax.set_ylim([np.max([0.98,np.min(detrend[good])-0.001]),np.min([np.max(detrend[good])+0.001,1.01])])      
                    ax.set_xlabel('Phase (P=' + str(np.round(best_p[cnt],decimals=4)) + ' days, ' +str(np.round(detsig[cnt],decimals=1)) + '-sigma)') 
                    ax.set_ylabel('Relative Brightness')
                    xlab = ax.xaxis.get_label()
                    ylab = ax.yaxis.get_label()
                    xlab.set_weight('bold')
                    xlab.set_size(12)
                    ylab.set_weight('bold')
                    ylab.set_size(12)
                    [i.set_linewidth(2) for i in ax.spines.itervalues()]
                    ax.xaxis.set_tick_params(width=2,labelsize=10)
                    ax.yaxis.set_tick_params(width=2,labelsize=10)
                    plt.savefig(figdir+'detections/' + 'phase_EPIC'+str(epic)+'_'+str(cnt)+'.pdf')
                    plt.clf()
        
        ##if saving don't return anything other than the filename where the results where saved
        
        return savefile,detection

##Function that injects a transiting planet into detrended data
##note that it takes the lightcurve, period,rp (earth radii),impact param,t0, e,little-omega and the hosts mass/radius
##semimajor axis computed from period+star mass/radius,
##inclination computed from impact parameter and semimajor axis
def construct_planet_signal(time,per,rp,b,t0,ecc,omega,mstar,rstar,oversample=20,exposure_time=30.0,ldpars=[0.4,0.3]):
    
    ##exposure time in days
    time_exp = exposure_time/60.0/24.0
    
    ##convert planet radiuss in earth units to rp/rs
    rearth = 0.009154
    rprs = rp*rearth/rstar
    
    #convert to inclination and semimajor axis from period and impact_par using rstar,mstar
    semia = (mstar*(per/365.25)**2)**(1.0/3.0)*214.939469384/rstar
    inc   = np.arccos(b/semia)*180.0/np.pi ##in degrees as batman likes it
    #tdur  = per/np.pi*np.arcsin(np.sqrt(((1+rprs)**2 - b**2)/(1-np.cos(inc*np.pi/180)**2))/semia)*24.0 ##for testing
    
    ##set batman parameters
    params           = batman.TransitParams()
    params.t0        = t0
    params.per       = per
    params.rp        = rprs
    params.a         = semia
    params.inc       = inc
    params.ecc       = ecc
    params.w         = omega
    params.limb_dark = 'quadratic'
    params.u         = ldpars

    ##initialize batman
    batman_model = batman.TransitModel(params,time,supersample_factor=oversample,exp_time=time_exp)
    
    ##calculate the light curve
    batman_lc    = batman_model.light_curve(params)

    ##output the light curve
    return batman_lc


    
##script to inject a planet transit signal to the Vanderburg raw photometry and attempt 
##to recover it.
def injrec_main(epic,cnum,rawdata,per,rp,impact,t0,ecc,omega,mstar,rstar,idstring='',known_period=[-1,-1],known_period2=[-1,-1],wsize=1.0,k2sff_ndays=1.5,k2sff_ndaysfirst=1.5,k2sff_gradual=False,demode=1,forcenull=False,min_period=1.000001,max_period=30.0,alias_num=2.0,thelogfile=None):
    urawdata     = rawdata.copy() ##duplicate it so that it doesn't get overwritten by the injection
    if thelogfile != None: 
        thelogfile.write('loaded raw \n')
        thelogfile.flush()
    #find any nans in time
    qwe      = np.where(np.isnan(urawdata.t) == False)[0]
    qwe2     = np.where(np.isnan(urawdata.t))[0]
    ##The planet signal:
    injflux  = construct_planet_signal(urawdata.t[qwe],per,rp,impact,t0,ecc,omega,mstar,rstar,oversample=20,exposure_time=scipy.stats.mode(urawdata.t[qwe]-np.roll(urawdata.t[qwe],1))[0][0]*24.0*60.0)
    ijf = np.ones(len(urawdata.t),dtype=float)
    ijf[qwe] *= injflux
    #injflux[qwe2] = 1.0
    ##figure out the expected depth from the batman lightcurve, important when impact parameter is closer to 1.0
    exp_depth    = 1.0-np.min(injflux)
    
   
    ##now inject the planet signal into the rawdata aperture photometry, this happens to be easy
    urawdata.fpsf  *= ijf
    urawdata.fcirc *= ijf

    if thelogfile != None: 
        thelogfile.write('created model \n')
        thelogfile.flush()    
    
    ##now run k2sff on the injected aperture photometry
    injdata,dddd,tcut,fcorcut,frawcut,flag = k2sff.reducek2_rawrev2rev(urawdata,cnum=cnum,
                            numdays=k2sff_ndays,numdaysfirst=k2sff_ndaysfirst,gradual=k2sff_gradual,idstring=idstring,thelogfile=thelogfile)

    if thelogfile != None: 
        thelogfile.write('ran k2sff \n')
        thelogfile.flush()
    
    ##print some stuff to show where we got to
    #ijmlf = open('logfiles/testingfile.txt','ab')
    #ijmlf.write(idstring +' '+ str(per) + ' ' + str(rp) + ' \n')
    #ijmlf.flush()
    #ijmlf.close()

    ##now run our notch detrending with the period matching keyword set 
    if forcenull == False:usedeltabic=-1 
    if forcenull == True:
        usedeltabic = np.inf
        print 'forcing null model'
    detected_it,detp,dett0,detdp = do_detrend(-1,epic,arclength=False, raw=False, wsize=wsize,saveoutput=False,resolvabletrans=False,k2sff=False,idstring=idstring,indata=injdata,
                    known_period=known_period,known_period2=known_period2,period_matching = [per,exp_depth,t0],
                    demode=demode,deltabic=usedeltabic,min_period=min_period,max_period = max_period,alias_num=alias_num)
    if thelogfile != None: 
        thelogfile.write('ran detrend_k2 \n')
        thelogfile.flush()
    return detected_it,detp,dett0,detdp


##special injection testing for tess, doesn't need K2SFF
def injrec_tess_test(epic,rawdata,per,rp,impact,t0,ecc,omega,mstar,rstar,idstring='',wsize=1.0,demode=1,forcenull=False,alias_num=2.0,min_period=1.0001,max_period=12.0):
    
    ##rawdata should be the input data after K2SFF processing.
    
    urawdata     = rawdata.copy() ##duplicate it so that it doesn't get overwritten by the injection
    
    #find any nans in time
    qwe      = np.where(np.isnan(urawdata.t))[0]
    ##The planet signal:
    injflux  = construct_planet_signal(urawdata.t,per,rp,impact,t0,ecc,omega,mstar,rstar,oversample=20,exposure_time=scipy.stats.mode(urawdata.t-np.roll(urawdata.t,1))[0][0]*24.0*60.0)
    injflux[qwe] = 1.0
    ##figure out the expected depth from the batman lightcurve, important when impact parameter is closer to 1.0
    exp_depth    = 1.0-np.min(injflux)
    
   
    ##now inject the planet signal into the rawdata aperture photometry, this happens to be easy
    urawdata.fcor *= injflux
    urawdata.fraw *= injflux

    ##now run our notch/LCR detrending with the period matching keyword set 
    if forcenull == False: usedeltabic=-1.0
    if forcenull == True : usedeltabic = np.inf
    
    ##these outputs are a 1/0 flag, the period, and the t0
    detected_it,detp,dett0,detdp = do_detrend(-1,epic,arclength=False, raw=False, wsize=wsize,saveoutput=False,resolvabletrans=False,k2sff=False,idstring=idstring,indata=urawdata,period_matching = [per,exp_depth,t0],demode=demode,deltabic=usedeltabic,min_period=min_period,max_period = max_period,alias_num=alias_num)
    #print 'done1'
#    if exp_depth <= 0.0001:
#        if detected_it == True: print 'Strange!!!!,'+idstring
#    
    return detected_it,detp,dett0,detdp


##basic injrec testing loop    
def loop_injrec(epic,cnum):  
    ##first check if a k2sff reduction has been done at all, if not, run k2sff on the tpf files
    if os.path.isfile(os.getenv("HOME")+'/python/projects/K2pipe/k2data/rawrev/'+'ktwo' + str(epic)+ 'extr.idl') == False:
        print 'No k2sff reduction products found, running it now'
        dummy = run_k2sff_idl(epic,cnum,raw=True,injrec=False)
    else: print 'Found a k2sff raw file for this EPIC'
    
    ##Then check for a basic detrend to start with:
    if os.path.isfile('k2data/init_detrend/detrend_EPIC'+str(epic)+'.pkl') == False:
        print 'Initial Detrend not found, running now'
        resultfile,detection = do_detrend(cnum,epic,arclength=False, raw=False, wsize=1.0, data_dir='k2data/rev/',outdir='k2data/init_detrend/',saveoutput=True,resolvabletrans=False,k2sff=True)
    else: 
        resultfile = 'k2data/init_detrend/detrend_EPIC'+str(epic)+'.pkl'
        print 'Found an initial detrend for this EPIC'
    ##now restore the initial detrending
    data,fittimes,depth,detrend,polyshape,badflag,pgrid,power,phase,FAM,detsig,pbest,dp = pickle.load(open(resultfile,'rb'))

    ##build a grid of planet parameters to inject, really it should be period (1.0-20.0) and depth(0.0005
    pinj_vect = np.linspace(1.0,20.0,10)
    dinj_vect = np.linspace(0.0001,0.005,20)
#    dur_grid  = np.array([0.75,1.0,2.0,3.0,4.0])##hours
    dur_grid   = np.array([2.0]) ##a dummy version for tests
    phase_grid = np.linspace(0.01,0.99,10)
    phase_grid = np.array([0.4,0.8]) ## a dummy for testing
    ##somewhere to save everything
    ijgrid = np.zeros((len(pinj_vect),len(dinj_vect),len(dur_grid),len(phase_grid),5))
    for pp in range(len(pinj_vect)):
        for dd in range(len(dinj_vect)): 
            thisrprs = np.sqrt(dinj_vect[dd])
            for du in range(len(dur_grid)):
                thisdur = dur_grid[du]
                for ph in range(len(phase_grid)):
                    thisdetect = injrec_main(epic,cnum,data,detrend,pinj_vect[pp],thisrprs,thisdur,phase_grid[ph]*pinj_vect[pp]+data.t[0])
                    wedet = 0
                    if thisdetect == True: wedet = 1 
                    ijgrid[pp,dd,du,ph,:] = np.array([pinj_vect[pp],dinj_vect[dd],thisdur,phase_grid[ph],wedet])
    pickle.dump((ijgrid,epic,cnum),open('k2data/injrec_outputs/EPIC'+str(epic)+'_ijgrid.pkl','wb'))
    return 1

##function that checks everything required for injrec loop exists

def check_preprocess(epic,cnum):  
    ##first check if a k2sff reduction has been done at all, if not, run k2sff on the tpf files
    if os.path.isfile(os.getenv("HOME")+'/python/projects/K2pipe/k2data/rawrev/'+'ktwo' + str(epic)+ 'extr.idl') == False:
        print 'No k2sff reduction products found, running it now'
        dummy = run_k2sff_idl(epic,cnum,raw=True,injrec=False)
    else: print 'Found a k2sff raw file for this EPIC'
    
    ##Then check for a basic detrend to start with:
    if os.path.isfile('k2data/init_detrend/detrend_EPIC'+str(epic)+'.pkl') == False:
        print 'Initial Detrend not found, running now'
        resultfile,detection = do_detrend(cnum,epic,arclength=False, raw=False, wsize=1.0, data_dir='k2data/rev/',outdir='k2data/init_detrend/',saveoutput=True,resolvabletrans=False,k2sff=True)
    else: 
        resultfile = 'k2data/init_detrend/detrend_EPIC'+str(epic)+'.pkl'
        print 'Found an initial detrend for this EPIC'
        
        data,fittimes,depth,detrend,polyshape,badflag,pgrid,power,phase,FAM,detsig,pbest,dp = pickle.load(open(resultfile,'rb'))
    return data,detrend

def check_preprocess_maverick(epic,basedir):
    ##first check if a k2sff reduction has been done at all, if not, run k2sff on the tpf files
    if os.path.isfile(basedir + 'k2data/rawrev/'+'ktwo' + str(epic)+ 'extr.idl') == False:
        print 'No k2sff reduction products found'
        return -1
    else: 
        print 'Found a k2sff raw file for this EPIC'
        return 1
        

def injrec_list_mpi(epic,cnum,pointlist,rawdata,mstar,rstar,ids,thisrank=-1,machinename='laptop',jobname='',
                    known_period=[-1,-1,-1,-1],known_period2=[-1,-1,-1],wsize=1.0,k2sff_ndays=1.5,
                    k2sff_ndaysfirst=1.5,k2sff_gradual=False,demode=1,forcenull=False,alias_num=2.0,min_period=1.000001,max_period=30.0,dontlog=False):
    '''
    Function for running injection recovery over a list of planet parameters for injection. Lots of options that can be set.
    Inputs: 
        (1) epic : epic number,  integer
        (2) cnum : k2 campaign number , integer   
        (3) pointlist: list on 10-lenght numpy arrays that contain the planet parameters for injection, see tacc_injrec.py for details
        (4) rawdata: the aperture photometry raw data in k2sff format
        (5) mstar: stars mass in solar units
        (6) rstar: stars radius in solar units
        (7) ids: job id string, good for keeping track of cores
        
    Optional inputs: 
        (1) thisrank=-1: integer of the current core rank, -1 means not a multiprocess
        (2) machinename='laptop': machine identifier, default is laptop that means not tacc, used for deciding logging 
        (3) jobname='': name of job for tracking log files
        (4) known_period=[-1,-1,-1,-1]. Mostly ignore this
        (5) known_period2=[-1,-1,-1]. Mostly ignore this
        (6) wsize=1.0: notch filter window size for this star
        (7) k2sff_ndays: spline breakpoint spacing for k2sff, ending spacing if k2sff_gradual==True
        (8) k2sff_ndaysfirst: spline breakpoint spacing to start at in k2sff_gradual==True, does nothing k2 k2sff_gradual==False
        (9) demode=1: detrending mode, 1=notch filter, 2=LOCoR
        (10) forcenull=False: force null model in notch filter if demode ==1
        (11) alias_num: Minimum Pseudo-Period for LOCoR.
        (13) max_period=30.0   : Miaximum period to search in BLS
        (14) dontlog=False: If True, no logs will be written
    Outputs: 
        pointlist: pointlist with the detection flag elements filled in
        errorcode: -1 means that something failed in the injection-recovery, 1 means everything should be fine. Handled by tacc_injrec.py  
    '''
    homedir = os.getenv("HOME")
    ##open a log file to write to, not efficient but maybe worth it for bugshooting
    if dontlog == False:
        if machinename == 'laptop': logfile = open('logfiles/logfile_main.txt','ab')
        if machinename != 'laptop': logfile = open('/work/03602/arizzuto/lonestar/logfiles/logfile_'+jobname+'_' + str(thisrank)+'.txt','ab')
        logfile.write(str(thisrank) + ' ' + str(epic) + ' \n')
        logfile.flush()
    
    
    for i in range(pointlist.shape[0]):
        if dontlog == False:
            logfile.write(str(i)+'_'+str(thisrank) +' ' +str(pointlist[i,0]) + ' ' +str(pointlist[i,1])+ ' ' +str(pointlist[i,2])+ ' ' +str(pointlist[i,3]) + ' ' +str(pointlist[i,4]) + ' ' +str(pointlist[i,5]) + ' ' +str(pointlist[i,6]) + '')
            logfile.flush()
        
        ##run the inject_test on this particular planet, wrap in error catcher so that exceptions can be diagnostic
        ##not we also need a timeout exception to make sure that the code/node doesn't get stuck in IO or something
        signal.signal(signal.SIGALRM,timeout_handler)
        signal.alarm(10*60)##timeout in seconds for a SINGLE injection test, not the full suite in pointlist, lets do 10 mins, it generally takes 5-10 mins for 3 tests or so
        ##The code with exception catch
        try:
            if dontlog == True: logfile = None
            thisdetect,detp,dett0,detdp = injrec_main(epic,cnum,rawdata,pointlist[i,0],pointlist[i,1],pointlist[i,2],pointlist[i,3]*pointlist[i,0]+rawdata.t[0],pointlist[i,4],
                                pointlist[i,5],mstar,rstar,idstring=ids,known_period=known_period,known_period2=known_period2,wsize=wsize,
                                k2sff_ndays=k2sff_ndays,k2sff_ndaysfirst=k2sff_ndaysfirst,k2sff_gradual=k2sff_gradual,demode=demode,forcenull=forcenull,alias_num=alias_num,min_period=min_period,max_period=max_period,thelogfile=logfile)
        except Exception,theexc: ##return the information needed to figure out what went wrong, and the error flag
            thestring = str(pointlist[i,0])+' ,'+str(pointlist[i,1])+' ,'+str(pointlist[i,2])+' ,'+str(pointlist[i,3])+' ,'+str(pointlist[i,4])+' ,'+str(pointlist[i,5])+' ,'+str(pointlist[i,6])
            thestring = 'rank:'+ids+' PPARS:'+thestring + '_'+str(epic) + '_'+str(wsize) + '_'+str(demode) +'_'+ str(mstar) +'_'+str(rstar) + '_' + str(k2sff_ndays) + '_' + str(k2sff_ndaysfirst) + '_' + str(k2sff_gradual) + '__' + str(theexc)
            #print thestring
            return thestring, -1
        signal.alarm(0) ##cancel timer if all is fine
        
        ##make it known we finished this test in the logs
        if dontlog == False:
            logfile.write('ddd \n')
            logfile.flush()
            
       ##Everything is fine, output the right way
        wedet=0
        if thisdetect == True:  wedet = 1
        pointlist[i,6] = wedet
        pointlist[i,7] = detp
        pointlist[i,8] = dett0
        pointlist[i,9] = detdp
    
    if dontlog == False: logfile.close
    return pointlist, 1
    
##looping function for tess testing injrec
def injrec_tesslist_mpi(epic,pointlist,rawdata,mstar,rstar,ids,thisrank=-1,machinename='laptop',jobname='',
                    wsize=1.0,demode=1,forcenull=False,alias_num=2.0,min_period=1.0001,max_period=12.0):
    ##open a log file to write to, not efficient but maybe worth it for bugshooting
    homedir = os.getenv("HOME")
    
    if machinename == 'laptop': logfile = open('logfiles/logfile_main.txt','ab')
    if machinename != 'laptop': logfile = open(homedir +'/project/K2pipe/logfiles/logfile_'+jobname+'_' + str(thisrank)+'.txt','ab')
    logfile.write(str(thisrank) + ' ' + str(epic) + ' \n')
    logfile.flush()
    for i in range(pointlist.shape[0]):
    #for i in range(1): ##for testing

        logfile.write(str(i)+'_'+str(thisrank) +' ' +str(pointlist[i,0]) + ' ' +str(pointlist[i,1])+ ' ' +str(pointlist[i,2])+ ' ' +str(pointlist[i,3]) + ' ' +str(pointlist[i,4]) + ' ' +str(pointlist[i,5]) + ' ' +str(pointlist[i,6]) + ' \n')
        logfile.flush()
        
        ##run the inject_test on this particular planet, wrap in error catcher so that exceptions can be diagnostic
        ##The code with exception catch
        
        thisdetect,detp,dett0,detdp = injrec_tess_test(epic,rawdata,pointlist[i,0],pointlist[i,1],pointlist[i,2],pointlist[i,3]*pointlist[i,0]+rawdata.t[0],pointlist[i,4],
                           pointlist[i,5],mstar,rstar,idstring=ids,wsize=wsize,demode=demode,forcenull=forcenull,alias_num=alias_num,min_period=min_period,max_period=max_period)
        # except Warning: ##return the information needed to figure out what went wrong, and the error flag
#             thestring = str(pointlist[i,0])+' ,'+str(pointlist[i,1])+' ,'+str(pointlist[i,2])+' ,'+str(pointlist[i,3])+' ,'+str(pointlist[i,4])+' ,'+str(pointlist[i,5])+' ,'+str(pointlist[i,6])
#             thestring = 'rank:'+ids+' PPARS:'+thestring
#             return thestring, -1
#         ##make it known we reached the last one
        logfile.write('ddd')
        logfile.flush()
            
       ##Everything is fine, output the right way
        wedet=0
        if thisdetect == True:  wedet = 1
        pointlist[i,6] = wedet
        pointlist[i,7] = detp
        pointlist[i,8] = dett0
        pointlist[i,9] = detdp
    
    logfile.close
    return pointlist, 1

def make_transitstamp_interactive(starname,time,detrend,bflag,period,t0,outdir='transitstamps/'):
    from gaussfit import gaussfit_mp
    from plotpoint import plotpoint
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    
##set matplotlibglobal params
    mpl.rcParams['lines.linewidth']   = 1.5
    mpl.rcParams['axes.linewidth']    = 2
    mpl.rcParams['xtick.major.width'] =2
    mpl.rcParams['ytick.major.width'] =2
    mpl.rcParams['ytick.labelsize'] = 15
    mpl.rcParams['xtick.labelsize'] = 15
    mpl.rcParams['axes.labelsize'] = 18
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['axes.labelweight']='semibold'
    mpl.rcParams['font.weight'] = 'semibold'


    
    phase = calc_phase(time,period,t0)
    phasetime = (phase-0.5)*period
    extracut = np.percentile(detrend,99.9)##don't plot the highest 0.1% of points, they are either garbage or not important for us now
    extracut2 = np.percentile(detrend,100.-99.9)
    lcrms = np.sqrt(np.nanmean((1.0-detrend[np.where((detrend < extracut) & (detrend > extracut2))[0]])**2))
    good = np.where((bflag < 2) & (detrend >0.1) & (detrend < 1.5*lcrms + 1) & (detrend < extracut))[0]
    

    
    ##now plot things up for the user to interact with using the plotpoint class
    print 'Click start of Ingress and end of egress'
    lpoint = plotpoint(phase[good],detrend[good])
    lpoint.getcoord()
    ingress = (lpoint.clickpoints[0][0] - 0.5)*period
    egress  = (lpoint.clickpoints[1][0] - 0.5)*period


    xrange = np.max([np.absolute(ingress),np.absolute(egress)])
    inx = np.where((phasetime[good] < xrange) & (phasetime[good] > -xrange))[0]
    

    startdep = 1.0 - np.min(detrend[good[inx]])
    gfit0 =np.array([startdep,0.0001,xrange,1.0,0.0,0.0])
    gfitpars,gfitcov,gfit = gaussfit_mp(phasetime[good],detrend[good],detrend[good]*0.01,gfit0,nocurve=True)
    fitdepth = gfitpars[0]

    
    ##now readjust the yrange and in-transit region again
    inx = np.where((phasetime[good] < xrange*2.0) & (phasetime[good] > -xrange*2.))[0]
    if len(inx) == 0:pdb.set_trace()
    binlc,binsize,ebin,goodind,binphase = k2sff.lcbin(phasetime[good[inx]],detrend[good[inx]],np.min([len(inx)/5,40]),userobustmean=True)    
    #pdb.set_trace()
    stdep = 1-np.min(detrend[good[inx[goodind]]])
    yrange = [1.0 - stdep*1.1,1.5*lcrms+1]  ##for now this is probably fine
    if xrange*2 >= period/2.0: yrange = [np.min(detrend),yrange[1]]
    if starname == '211093684': yrange = [np.min(detrend),np.max(detrend)]

    #import pdb
    #pdb.set_trace()

    perstring = str(np.round(period,decimals=3))[0:5] + ' days'
    perstring2 = str(np.round(period,decimals=3))[0:5] + 'days'

    fig,ax = plt.subplots(1)
    ax.plot(phasetime[good]*24.,detrend[good],'ok',markersize=5)
    if np.max(phasetime[good[inx]]) - np.min(phasetime[good[inx]]) < period/2: ax.plot(binphase*24.,binlc,'ro',markeredgecolor='r',markersize=10) ##only plot the binned LC for planets not EB's
    ax.set_xlim([np.max([-xrange*2,np.min(phasetime[good])])*24,np.min([xrange*2,np.max(phasetime[good])])*24])
    ax.set_ylim(yrange)
    ax.set_ylabel('Relative Brightness')
    ax.set_xlabel('Phased Time (Hours)')
    ax.text(0.94, 0.05,'EPIC '+starname + ' ' + 'P='+perstring,horizontalalignment='right', verticalalignment='top',fontweight='bold',transform = ax.transAxes,backgroundcolor='w')
    ax.get_yaxis().get_major_formatter().set_scientific(False)
    ax.get_yaxis().get_major_formatter().set_useOffset(False)
    plt.tight_layout()
    plt.savefig(outdir + 'EPIC'+starname+'_' + perstring2 +'_transitstamp.pdf')
    
    
    plt.show()
    plt.cla()
    plt.clf()
    
    plt.close("all")
    donefig = 1
    

    
def lomb_the_scargle(time,flux,prange=[0.1,5.0],snrcut = 5.0,fullreturn=False):
    '''
    function to do a lomb-scargle periodogram and figure out the rotation period
    from a light curve
    '''
    from astropy.stats import LombScargle
    import astropy.units as u
    import scipy.signal
    pssize = 10000
    fbin,binsize,ebin,allgoodind,tbin = k2sff.lcbin(time,flux,60,usemean=False,userobustmean=True,linfit=False)
    lspgram  = LombScargle(time[allgoodind],flux[allgoodind])
    freqs    = np.linspace(1.0/prange[1],1.0/prange[0],pssize)
    freqs,lspower = lspgram.power(freqs)
    
    stepsize = (1/prange[0]-1/prange[1])/pssize*2
    ks = np.round(1.0/stepsize).astype(int)
    if np.mod(ks,2) == 0: ks +=1
    mfiltpower = scipy.signal.medfilt(lspower,kernel_size=ks)
    
    cleansig = lspower-mfiltpower ##dividing can create peaks in weird cases
    #import pdb
    #import matplotlib.pyplot as plt
    
    rms = np.sqrt(np.mean((np.median(cleansig)-cleansig)**2))
    mad = np.median(np.absolute(np.median(cleansig)-cleansig))
    snr = cleansig/rms
    ##now find peaks
    peakind = np.argmax(snr)
    peaksnr = snr[peakind]

        
    #pdb.set_trace()
    period  = 1/freqs[peakind]
    if peaksnr < snrcut: period = 1000
    print 'Period is ' + str(period) + ' days'

    import pdb
    import matplotlib.pyplot as plt
    plt.plot(1/freqs,snr)
    plt.show()
    pdb.set_trace()
    
    #plt.plot(1/freqs,cleansig)
    #plt.plot(1/freqs[peakind[thepeak]],cleansig[peakind[thepeak]],'ro')  
    #pdb.set_trace()
    if fullreturn == True: return cleansig,freqs,period,peakind,snr
    return period