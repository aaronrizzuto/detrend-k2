##standard imports (not necessarily all used)
import numpy as np
import sys,os,pdb,pickle
import matplotlib.pyplot as plt
from scipy.io.idl import readsav as idlread
import detrend_k2
import k2sff

##load in the data
##the standard format for the input data to the pipeline is a 
##numpy record array with the following fields:
##t = time in days
##fcor = flux corrected for k2sff (put in your flux here)
##fraw = raw flux (just put the same as fcor for non-k2 purposes)
##s    = arclength (just put 0 for non k2)
##qual = a quality flag( just put 0 for all points)
##divisions = where k2sff cut the lightcurve into sections for  the arclength fit (put all 0 for non k2)
##detrend   = just leave it all as zeros

#to make your own input data from something, use this
thedata = np.recarray((600,), dtype=[('t',float),('fraw',float),('fcor',float),('s',float),('qual',int),('detrend',float),('divisions',float)])
thedata.t = np.arange(600)*1.0
thedata.fcor = np.sin(np.arange(600)/50.)+1
thedata.fraw = np.sin(np.arange(600)/50.)+1
thedata.s *=0.0
thedata.divisions+=0
thedata.qual*=0
thedata.detrend*=0

##load the example data provided
thedata  = pickle.load(open('example_data/k233_example.pkl','rb'))
thisepic = 205117205 ##just an identifying number
thisprot = 6.3 ##I think this is right
cnum     = 2 ##the k2 campaign number, not really important

##set some parameters for the detrending
demode,alias_num = 1,2.0 ##this is notch
##demode,alias_num=2,2.0 ##this is locor, if running this, also set the period alias, default is 2 for k2

thiswindowsize = 1.0 ##notch filter window size if using LCR, this is the rotation period

##the notch model bayesian information criterion threshold:
deltabic = 0.0 ##set to -np.inf to accept all notches, and np.inf to force null model everywhere

##BLS parameters, these are the defaults
snrcut=7.0
max_period=30.0
min_period=1.00001

##define output directories, only needed for saveoutput=True
outdir     = 'testing_output/'
chechdir = outdir

if demode == 2: outdir     = 'testing_output_lcr/'

##check the directory structure is there, just assume if the root is present everything is
##if not present, make them all
if os.path.exists(outdir) == False: 
    os.makedirs(outdir)
    os.makedirs(outdir + 'figures/')
    os.makedirs(outdir + 'figures/' + 'detections/')

##if running with saveoutput = False, then do this, good for real time playing
data,fittimes,depth,detrend,polyshape,badflag,pgrid,firstpower,firstphase,detsig,best_p,dp,t0,dcyc = detrend_k2.do_detrend(cnum,thisepic,wsize=thiswindowsize,indata=thedata,saveoutput=False,outdir=outdir,alias_num=alias_num,demode=demode,deltabic=deltabic,max_period=max_period,min_period=min_period,snrcut=snrcut)

##plot the detrended lightcurve
plt.plot(data.t,detrend,'k.')
bad = np.where(badflag == 2)[0]
plt.plot(data.t[bad],detrend[bad],'r.')
plt.xlabel('Time (days)')
plt.ylabel('Relative Flux')

##plot the depth values:
plt.figure()
plt.plot(data.t,1-depth,'k.')
bad = np.where(badflag == 2)[0]
plt.plot(data.t[bad],1-depth[bad],'r.')
plt.xlabel('Time (days)')
plt.ylabel('Depth')

##plot the output phased to the best detection
phased = detrend_k2.calc_phase(data.t,best_p[0],t0[0])
plt.figure()
plt.plot(phased,detrend,'.k')
plt.plot(phased[bad],detrend[bad],'r.')
plt.xlabel('Time (phased to ' + str(best_p[0]) + ' days)')
plt.ylabel('Relative Flux')
plt.show()

pdb.set_trace()
##if running with saveoutput = True, then do this    which just returns the filename where the output was saved    
##good for processing lots of stuff 
#savefile,detection = detrend_k2.do_detrend(cnum,thisepic,wsize=thiswindowsize,indata=thedata,saveoutput=True,outdir=outdir,alias_num=alias_num,demode=demode,deltabic=deltabic,max_period=max_period,min_period=min_period,snrcut=snrcut)
#pdb.set_trace()


print "All Done!!"

















