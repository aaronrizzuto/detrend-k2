import numpy as np
import pickle
import glob,os,pdb,time
import matplotlib.pyplot as plt
import matplotlib as mpl
import detrend_k2
from readcol import readcol
mpl.rcParams['figure.figsize'] = 10,8
datestamp = time.strftime('%Y%m%d', time.localtime(time.time()))

auxmode=False

##put the output file directory for a group of detrended stars here and run in command line

datadir = 'path/to/detrended/files/'


basefiles = glob.glob(datadir+'detrend_EPIC*[0-9].pkl')
extratag = ''
if auxmode == True: extratag = '_divclipBLS'

plt.ion()
spc = ' '
dfname = datadir + 'detinspect'+extratag+'_'+datestamp+'.txt'
if os.path.isfile(dfname) == False:
    detfile = open(dfname,'ab')
    detfile.write('name'.ljust(13)+spc+ 'period'.ljust(7)+spc+ 'sigma'.ljust(5)+spc+ 'Dsig dp dcyc  t0  \n')
    detfile.flush()
else: detfile = open(dfname,'ab')


donefilename = glob.glob(datadir + 'inspected'+extratag+'*txt')
if len(donefilename) > 1:pdb.set_trace()
if len(donefilename) == 1: 
    donelist = readcol(donefilename[0])[:,0].astype(str)
   # pdb.set_trace()
    donefile = open(donefilename[0],'ab')
if len(donefilename) == 0: 
    donelist = np.array(['-1'])
    donefile = open(datadir + 'inspected'+extratag+'_'+datestamp+'.txt','wb')




##loop star by star
fig, (ax1,ax2,ax3) = plt.subplots(3)
#plt.tight_layout()

for i,file in enumerate(basefiles):
    starname = file.split('.')[0].split('_')[-1]
    fig.suptitle(starname + ' ' + str(i+1) +'/'+str(len(basefiles)))
    qwe = np.where(donelist == starname)[0]
    if len(qwe) > 1: pdb.set_trace()
    if len(qwe) < 1:
        #pdb.set_trace()
        data,fittimes,depth,detrend,polyshape,bflag,pgrid,power,phase,detsig,bestp,dp,t0,dcyc = pickle.load(open(file,'rb'))
        
        #pdb.set_trace()
        if auxmode == True: 
            otherfile = file.split('.pkl')[0] + extratag+'.pkl'
            pgrid,power,phase,detsig,bestp,dp,t0,dcyc = pickle.load(open(otherfile,'rb')) ##overwrite these things for the second search

        #pdb.set_trace()
        #bestp,dp,t0,detsig,firstpower,pgrid,firstphase,dcyc = detrend_k2.bls_transit_search(data,detrend,bflag)
       # pdb.set_trace()
        ##is there a detections
        
        this_detindex = np.where(detsig > 7.0)[0]
        #pdb.set_trace()
        print bestp[this_detindex]
        rper,rpindx = np.unique(np.round(bestp[this_detindex],3),return_index=True)
        this_detindex = this_detindex[rpindx]
        #pdb.set_trace()
        
        
        realmask = np.zeros(len(this_detindex),dtype=int)
        ##plot potential detections for use inspection
        if len(this_detindex) > 0:
            extracut = np.percentile(detrend,99.9)##don't plot the highest 0.1% of points, they are either garbage or not important for us now
            good = np.where((bflag < 2) & (detrend >0.5) & (data.fcor > 0.5) & (detrend < extracut))[0]
            ##calculate the general scatter for plotting later        
            rmsoff = np.sqrt(np.mean((detrend[good]-1)**2))
            g2 = np.where((detrend[good] <= 1+rmsoff*5.0) & (detrend[good] >= 1-rmsoff*5.0))[0]
           # pdb.set_trace()
            yrange = [np.max([1-rmsoff*5.0,np.min(detrend[good[g2]])]),np.min([rmsoff*5.0+1,np.max(detrend[good[g2]]),1.5])]
    
            for j in range(len(this_detindex)):
                det = this_detindex[j]
                thisphase = detrend_k2.calc_phase(data.t,bestp[det],t0[det])
                ax1.cla()
                ax2.cla()
                ax3.cla()
                ax1.plot(thisphase[good],detrend[good],'.')
                ax1.set_xlabel('Phased Time P=' + str(round(bestp[det],4))+ ' simga=' + str(round(detsig[det],2)))
                ax1.set_ylabel('Relative Brightness')
                ax1.set_ylim(yrange)
            
                tt = np.where((thisphase[good] < 0.6) & (thisphase[good] > 0.4))[0]
                ax2.plot(thisphase[good[tt]],detrend[good[tt]],'.')
                ax2.set_xlabel('Phased Time P=' + str(round(bestp[det],4))+ ' simga=' + str(round(detsig[det],2)))
                ax2.set_ylabel('Relative Brightness')
                ax2.set_ylim(yrange)
                intr = np.where((thisphase[good] < 0.52) & (thisphase[good] > 0.48))[0]

                ax3.plot(data.t[good],detrend[good],'.')
                ax3.plot(data.t[good[intr]],detrend[good[intr]],'.r')
                ax3.set_xlabel('Time (days)')
                ax3.set_ylabel('Relative Brightness')
                ax3.set_ylim(yrange)
                ax3.set_xlim([np.min(data.t[good]),np.max(data.t[good])])
            
                ##ask the user if they think it's real
                print starname + ' Period=' + str(round(bestp[det],4)) + ' Sigma='+str(round(detsig[det],2)) + ' DP=' + str(round(dp[det]*1000,2))
                var = raw_input("Real or Not (1/0)?")
                if var == '1': ##print output for this detection now
                    pline = starname+spc+str(round(bestp[det],4))+spc+str(round(detsig[det],2))+spc+str(round(detsig[det]*100,1))+spc+str(round(dp[det]*1000.0,1))+spc+str(round(dcyc[det]*1000.0,1))+spc+str(round(t0[det],4))+' \n'
                    detfile.write(pline)
                    detfile.flush()
        donefile.write(starname +' \n')  
    else: print starname + ' has already been inspected! Skipping....'
    
    
detfile.close()
donefile.close()

pdb.set_trace()
print 'All Done'




