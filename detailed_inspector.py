import numpy as np
from readcol import readcol
import matplotlib.pyplot as plt
import matplotlib as mpl
import pdb,sys,os,glob
import pickle
import detrend_k2
mpl.rcParams['figure.figsize'] = 12,8

auxmode=0
rerun_locor=True
rerun_locor=False
start = 9

##Run this after running inspector.py to look closely at the detections that you passed

datadir = 'path/to/detrended/files/'




dfile = glob.glob(datadir+'detinspect*')
if auxmode == 1: dfile = glob.glob(datadir+'detinspect_divclipBLS*')
detinfo = readcol(dfile[0],asRecArray=True)
meminfo = readcol('membership/pl_fulloutput_membership.csv',fsep=',',asRecArray=True) ##also needed for locor rerun

##to check something specific out, fill this in#
#qwe = np.where(detinfo.name == 'EPIC'+'210993561')[0] 
#start = qwe[0]
#pdb.set_trace()

for i in range(start,len(detinfo)):
    starname = int(detinfo.name[i].split('C')[1])
    mmm= np.where(meminfo.EPIC == starname)[0]
    fig,(ax1,ax2,ax3,ax4) = plt.subplots(4)
    fig.subplots_adjust(hspace=.3)
    #fig2,ax = plt.subplots(1)


    ax1.cla()
    ax2.cla()
    ax3.cla()
    ax4.cla()
    #ax.cla()
    fig.suptitle(detinfo.name[i] +'  P='+str(detinfo.period[i]))
    loaderfile = datadir+'detrend_'+detinfo.name[i]+'.pkl'
    data,fittimes,depth,detrend,polyshape,bflag,pgrid,firstpower,firstphase,detsig,bestp,dp,t0,dcyc = pickle.load(open(loaderfile,'rb'))
    
 

    if auxmode == 1:
        otherfile = loaderfile.split('.pkl')[0]+'_divclipBLS.pkl'
        pgrid,power,phase,detsig,bestp,dp,t0,dcyc = pickle.load(open(otherfile,'rb')) ##overwrite these things for the second search
        #pdb.set_trace()
    
    ##find the right phase
    qwe = np.argmin(np.absolute(bestp-detinfo.period[i]))
    thist0 = t0[qwe]

    ##force a period/t0
    #detinfo.period[i] = detinfo.period[i]*2
    #thist0 = 2235.29 + detinfo.period[i]/2
    #thist0 = 3006.0 + 0.5*detinfo.period[i]# + 0.05*detinfo.period[i]
    #thist0 = thist0 +0.1*detinfo.period[i]
    phase = detrend_k2.calc_phase(data.t,detinfo.period[i],thist0)
    if rerun_locor == True:
        print 'rerunning locor with membership list period'
        thisprot = meminfo.prot[mmm]
        cleanmsk = np.zeros(len(data.t),dtype=int)
        zxc = np.where((phase<0.51) & (phase > 0.49))[0]
        cleanmsk[zxc]=99
        time, dummy, detrend2, finalmodel2, badflag2 = detrend_k2.rcomb(data,thisprot,rmsclip=3.0,aliasnum=2.0,cleanmask=cleanmsk)
        odetrend = detrend*1.0
        detrend=detrend2*1.0
        
    ##special here for inspection, rerun notch-filter, nice for checking a locor target, can cleanmask out things too if you like
   #fittimesnf, depthstorenf, detrendnf, polyshapenf, badflagnf = detrend_k2.sliding_window(data,windowsize=0.5,resolution = 0.25, use_arclength=False, efrac=1e-3,use_raw=False,cleanmask=[-1,-1],deltabic=1.0)
    #pdb.set_trace()

    ##the transit region
    intr = np.where((phase < 0.52) & (phase > 0.48))[0]

    extracut = np.percentile(detrend,99.9)
    extracut2 = np.percentile(detrend,0.001)
    good = np.where((bflag < 2) & (detrend < extracut) & (detrend > extracut2))[0]
    ##calculate the general scatter for plotting later        
    rmsoff = np.sqrt(np.mean((detrend[good]-1)**2))
    yrange = [np.max([1-rmsoff*5.0,np.min(detrend[good])]),np.min([rmsoff*5.0+1,np.max(detrend[good])])]
        
    #pdb.set_trace()
    print i,len(detinfo),detinfo.name[i]   
    print detinfo.period[i], thist0 - detinfo.period[i]/2.0, detsig[qwe],dcyc[qwe]*detinfo.period[i]*24.,dp[qwe]*1000.0
    
    ##now plot diagnostic things
    ax1.ticklabel_format(useOffset=False)
    ax3.ticklabel_format(useOffset=False)
    ax2.ticklabel_format(useOffset=False)
    ax4.ticklabel_format(useOffset=False)
    ax1.plot(data.t[good],detrend[good],'.k')
    ax1.plot(data.t[intr],detrend[intr],'r.')
    ax1.set_ylim(yrange)
    ax1.set_ylabel('Relative Brightness')
    ax1.set_xlabel('Time (days)')
    ax2.plot(data.t[good],data.fraw[good],'.b')
    ax2.plot(data.t[good],data.fcor[good],'.k')
    ax2.plot(data.t[intr],data.fcor[intr],'r.')
    ax2.set_xlabel('Time (days)')
    ax2.set_ylabel('Relative Brightness')
    ax3.plot(data.t,data.s,'.k')
    ax3.plot(data.t[intr],data.s[intr],'.r')
    ax3.set_ylabel('Arclength (arcsec)')
    ax3.set_xlabel('Time (days)')
    ax4.plot(phase[good],detrend[good],'.k')
    ax4.plot(phase[intr],detrend[intr],'.r')
    if rerun_locor == True :ax4.plot(phase[good],odetrend[good],'g.',markersize=3)
    ax4.set_xlabel('Phased Time')
    ax4.set_ylabel('Relative Brightness')
    ax4.set_xlim([0.4,0.6])
    ax4.set_ylim(yrange)
    fig.savefig(datadir+'diagnostic_plots/'+detinfo.name[i]+'_diagnostic.pdf')

    #ax.plot(meminfo.radeg,meminfo.decdeg,'.k')
    #ax.plot(meminfo.radeg[mmm],meminfo.decdeg[mmm],'or')
    #pdb.set_trace()
    #ax.set_xlabel('RA (degrees)')
    #ax.set_ylabel('Decl. (degrees)')
    #fig2.savefig(datadir+'diagnostic_plots/'+detinfo.name[i]+'_positional.pdf')
    plt.show()
    #pdb.set_trace()
    



print 'All Done!!'












